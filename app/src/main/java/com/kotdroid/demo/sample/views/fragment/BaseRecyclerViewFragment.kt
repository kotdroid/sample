package com.kotdroid.demo.sample.views.fragment

import android.os.Handler
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kotdroid.demo.sample.R
import kotlinx.android.synthetic.main.fragment_base_recycler_view.*


abstract class BaseRecyclerViewFragment : BaseFragment() {

    override fun init() {
        // Set swipe refresh layout
        if (null != swipeRefreshLayout) {
            swipeRefreshLayout!!.setColorSchemeResources(R.color.colorAccent, R.color.colorAccent,
                    R.color.colorAccent, R.color.colorAccent)
            swipeRefreshLayout!!.setOnRefreshListener { onPullDownToRefresh() }
        }

        // Set recyclerView
        recyclerView.layoutManager = if (null == layoutManager) LinearLayoutManager(activity) else (layoutManager)
        if (isShowRecyclerViewDivider) {
            recyclerView.addItemDecoration(DividerItemDecoration(activity,
                    LinearLayoutManager.VERTICAL))
        }
        recyclerView.adapter = recyclerViewAdapter
        setData()

        // Observe swipe refresh layout
        viewModel?.isShowSwipeRefreshLayout()?.observe(this, Observer {
            if (it!!) {
                showSwipeRefreshLoader()
            } else {
                hideSwipeRefreshLoader()
            }
        })

        // Observe retrofit errors
        viewModel?.getRetrofitErrorDataMessage()?.observe(this, Observer {
            showNoDataText(it?.errorResId, it?.errorMessage)
        })
    }

    private fun showNoDataText(resId: Int? = null, message: String? = null) {
        if (null == resId && null == message) {
            hideNoDataText()
        } else {
            if (0 < recyclerViewAdapter?.itemCount!!) {
                showMessage(resId, message)
            } else {
                tvNoData?.visibility = View.VISIBLE
                tvNoData?.text = message ?: getString(resId!!)
            }
        }
    }

    private fun hideNoDataText() {
        tvNoData?.visibility = View.GONE
    }

    private fun showSwipeRefreshLoader() {
        swipeRefreshLayout?.post {
            if (null != swipeRefreshLayout) {
                swipeRefreshLayout!!.isRefreshing = true
            }
        }
    }

    private fun hideSwipeRefreshLoader() {
        Handler().postDelayed({
            if (null != swipeRefreshLayout && swipeRefreshLayout!!.isRefreshing) {
                swipeRefreshLayout!!.isRefreshing = false
            }
        }, 50)
    }



    abstract val recyclerViewAdapter: RecyclerView.Adapter<*>?

    abstract val layoutManager: RecyclerView.LayoutManager?

    abstract val isShowRecyclerViewDivider: Boolean

    abstract fun setData()

    abstract fun onPullDownToRefresh()

}