package com.kotdroid.demo.sample.views.fragment.camerasample

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.provider.MediaStore
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.utils.Constants
import com.kotdroid.demo.sample.utils.GeneralFunctions
import com.kotdroid.demo.sample.utils.VideoDownSampling
import com.kotdroid.demo.sample.viewmodels.BaseViewModel
import com.kotdroid.demo.sample.views.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_video_sampling.*
import java.io.File

class VideoSamplingFragment : BaseFragment() {

    companion object {
        const val REQUEST_TAKE_GALLERY_VIDEO = 67
    }

    override val layoutId: Int
        get() = R.layout.fragment_video_sampling

    override val isNavigationBarEnabled: Boolean
        get() = false

    override val viewModel: BaseViewModel?
        get() = null


    override fun init() {
        btnGetVideo.setOnClickListener {
            val intent = Intent()
            intent.type = "video/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_TAKE_GALLERY_VIDEO)
        }
    }

    override fun observeData() {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_TAKE_GALLERY_VIDEO && resultCode == RESULT_OK) {
            if (null != data?.data) {
                val inputVideoPath = getPath(data.data!!)

                try {
                    val vi = VideoDownSampling(activityContext)
                    vi.changeResolution(File(inputVideoPath), GeneralFunctions.setUpVideoFile(Constants.LOCAL_STORAGE_BASE_PATH_FOR_USER_VIDEOS)?.absolutePath)
                } catch (e: Exception) {

                }
            }
        }
    }

    // UPDATED!
    private fun getPath(uri: Uri): String? {
        val projection1 = arrayOf<String>()
        projection1.apply {
            MediaStore.Video.Media.DATA
        }

        val cursor = activity?.contentResolver?.query(uri, projection1, null, null, null)
        return when {
            cursor != null -> {
                // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
                // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
                val column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA)
                cursor.moveToFirst()
                return cursor.getString(column_index)

            }
            else -> null
        }
    }
}