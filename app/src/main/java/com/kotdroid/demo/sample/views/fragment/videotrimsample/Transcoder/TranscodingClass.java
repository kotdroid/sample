package com.kotdroid.demo.sample.views.fragment.videotrimsample.Transcoder;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Environment;
import android.widget.Toast;

import com.kotdroid.demo.sample.views.fragment.videotrimsample.GetTranscodedVideo;
import com.kotdroid.demo.sample.views.fragment.videotrimsample.Transcoder.androidtranscoder.MediaTranscoder;
import com.kotdroid.demo.sample.views.fragment.videotrimsample.Transcoder.androidtranscoder.format.MediaFormatStrategy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.fragment.app.Fragment;

/**
 * Created by cbl115 on 14/6/17.
 */

public class TranscodingClass {
	private Activity activity;
	private Fragment fragment;
	private String file;
	private String newFile;

	public TranscodingClass(Fragment fragment) {
		this.fragment = fragment;
		activity = fragment.getActivity();
	}

	private static MediaFormatStrategy createAndroid720pStrategy(int bitrate, int audioBitrate, int audioChannels) {
//		return new Android480pFormat(bitrate, audioBitrate, audioChannels);
		return new Android480pFormat();
	}

	public void setFile(String file) {
		this.file = file;
	}

	public void startTranscode(final ProgressDialog progressDialog) {
     /*   if (progressDialog != null) {
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setMessage(activity.getString(R.string.processing));
            progressDialog.setCancelable(false);
            progressDialog.setMax(100);
            progressDialog.setProgress(0);
            progressDialog.setIndeterminate(false);
            progressDialog.show();
        }*/
		MediaTranscoder.Listener listener = new MediaTranscoder.Listener() {
			@Override
			public void onTranscodeProgress(final double progress) {
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						progressDialog.setProgress((int) (progressDialog.getMax() * progress));
					}
				});
			}

			@Override
			public void onTranscodeCompleted() {
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						progressDialog.dismiss();
					}
				});
               /* if (file != null && new File(file).exists()) {
                    new File(file).delete();
                }*/
				GetTranscodedVideo getTranscodedVideo = (GetTranscodedVideo) fragment;
				getTranscodedVideo.getTranscoded(newFile, true);
			}

			@Override
			public void onTranscodeCanceled() {
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						progressDialog.dismiss();
					}
				});
			}

			@Override
			public void onTranscodeFailed(Exception exception) {
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						progressDialog.dismiss();
						Toast.makeText(activity, "video file is corrupted.", Toast.LENGTH_LONG).show();
					}
				});
			}
		};
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy_hh-mm-ss-SSS", Locale.ENGLISH);
			newFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
					.toString() +
					"temp_" + simpleDateFormat.format(new Date()) + ".mp4";
			MediaTranscoder.getInstance().transcodeVideo(file
					, newFile,
					createAndroid720pStrategy(3000 * 1000, 128 * 1000, Build.VERSION.SDK_INT > 20
							? 2 : 1), listener);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

