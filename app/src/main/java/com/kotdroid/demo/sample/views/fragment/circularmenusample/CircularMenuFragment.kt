package com.kotdroid.demo.sample.views.fragment.circularmenusample

import androidx.recyclerview.widget.LinearSnapHelper
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.viewmodels.BaseViewModel
import com.kotdroid.demo.sample.views.adapters.CircularAdapter
import com.kotdroid.demo.sample.views.fragment.BaseFragment
import com.kotdroid.demo.sample.views.utils.HalfCurveLayoutManager
import kotlinx.android.synthetic.main.activity_main1.*


class CircularMenuFragment : BaseFragment() {


    override val layoutId: Int
        get() = R.layout.activity_main1

    override val isNavigationBarEnabled: Boolean
        get() = false

    override val viewModel: BaseViewModel?
        get() = null

    override fun init() {


        val list = ArrayList<Int>().apply {
            add(R.drawable.emoji_one)
            add(R.drawable.emoji_two)
            add(R.drawable.emoji_three)
            add(R.drawable.emoji_four)
            add(R.drawable.emoji_five)
            add(R.drawable.emoji_six)
            add(R.drawable.emoji_seven)
            add(R.drawable.emoji_eight)
            add(R.drawable.emoji_nine)
            add(R.drawable.emoji_ten)
            add(R.drawable.emoji_one)
            add(R.drawable.emoji_two)
            add(R.drawable.emoji_three)
            add(R.drawable.emoji_four)
            add(R.drawable.emoji_five)
            add(R.drawable.emoji_six)
            add(R.drawable.emoji_seven)
            add(R.drawable.emoji_eight)
            add(R.drawable.emoji_nine)
            add(R.drawable.emoji_ten)
            add(R.drawable.emoji_one)
            add(R.drawable.emoji_two)
            add(R.drawable.emoji_three)
            add(R.drawable.emoji_four)
            add(R.drawable.emoji_five)
            add(R.drawable.emoji_six)
            add(R.drawable.emoji_seven)
            add(R.drawable.emoji_eight)
            add(R.drawable.emoji_nine)
            add(R.drawable.emoji_ten)
            add(R.drawable.emoji_one)
            add(R.drawable.emoji_two)
            add(R.drawable.emoji_three)
            add(R.drawable.emoji_four)
            add(R.drawable.emoji_five)
            add(R.drawable.emoji_six)
            add(R.drawable.emoji_seven)
            add(R.drawable.emoji_eight)
            add(R.drawable.emoji_nine)
            add(R.drawable.emoji_ten)

        }

        wrv.setCenterEdgeItems(true)
        val manager = HalfCurveLayoutManager(activityContext, 5.0f)
        wrv.layoutManager = manager
        val helper = LinearSnapHelper()
        // Set the adapter
        val adapter = CircularAdapter(activityContext, list)
        wrv.adapter = adapter
        helper.attachToRecyclerView(wrv)
    }

    override fun observeData() {}

}