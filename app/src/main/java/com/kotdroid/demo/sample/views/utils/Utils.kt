package com.kotdroid.demo.sample.views.utils

import android.content.res.Resources

object Utils {
    fun getPixelFromDp(dp: Float): Float {
        return dp * Resources.getSystem().displayMetrics.density
    }

    fun getDPFromPixel(pixel: Float): Float {
        return pixel / Resources.getSystem().displayMetrics.density
    }
}
