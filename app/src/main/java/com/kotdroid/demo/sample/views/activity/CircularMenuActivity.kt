package com.kotdroid.demo.sample.views.activity

import android.util.Log
import com.google.android.gms.tagmanager.TagManager
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.utils.ContainerHolderSingleton
import com.kotdroid.demo.sample.views.fragment.ECommerceAnalyticsFragment

class CircularMenuActivity : BaseAppCompactActivity() {
    override val layoutId: Int
        get() = R.layout.activity_home

    override val isMakeStatusBarTransparent: Boolean
        get() = false

    override fun init() {

        // Initialize GTM
//        val tagManager = TagManager.getInstance(this)
//        val pending = tagManager?.loadContainerPreferNonDefault("GTM-KPNGG3J", R.raw.gtm_kpngg3j_v1)
//
//        pending?.setResultCallback {
//            ContainerHolderSingleton.setContainerHolder(it)
//            val container = it.container
//            if (!it.status.isSuccess) {
//                Log.e("CuteAnimals", "failure loading container")
//            }
//        }



        doFragmentTransaction(containerViewId = R.id.flContainerHome, fragment = ECommerceAnalyticsFragment())
//        doFragmentTransaction(containerViewId = R.id.flContainerHome, fragment = KapilFragment())
//        doFragmentTransaction(containerViewId = R.id.flContainerHome, fragment = LondonEyeFragment())
//        doFragmentTransaction(containerViewId = R.id.flContainerHome, fragment = CircularMenuFragment())
    }
}