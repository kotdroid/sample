package com.kotdroid.demo.sample.utils

import android.os.AsyncTask
import androidx.fragment.app.Fragment
import com.iceteck.silicompressorr.SiliCompressor
import com.kotdroid.demo.sample.R
import java.net.URISyntaxException


class GetSampledVideo(private val mFragment: Fragment) : AsyncTask<String, Void, String>() {

    private var mSampledVideoAsyncResp: SampledVideoAsyncResp? = null
    private val mMyCustomLoader: MyCustomLoader by lazy { MyCustomLoader(mFragment.context) }

    init {
        mSampledVideoAsyncResp = mFragment as SampledVideoAsyncResp
    }

    override fun onPreExecute() {
        super.onPreExecute()
        mMyCustomLoader.showProgressDialog(mFragment.getString(R.string.processing_video))
    }


    override fun doInBackground(vararg params: String?): String {
        val videoPath = params[0]
        val videoDirectory = params[1]

        var filePath: String? = null
        try {

            filePath = SiliCompressor.with(mFragment.context).compressVideo(videoPath, videoDirectory)

        } catch (e: URISyntaxException) {
            e.printStackTrace()
        }

        return filePath!!


//        return MediaController.getInstance().convertVideo(videoPath, videoFile?.absolutePath)
    }


    override fun onPostExecute(path: String?) {
        super.onPostExecute(path)
        mMyCustomLoader.dismissProgressDialog()
        if (null != path && null != mSampledVideoAsyncResp) {
            mSampledVideoAsyncResp!!.onSampledVideoAsyncPostExecute(path)
        }
    }

    interface SampledVideoAsyncResp {
        fun onSampledVideoAsyncPostExecute(path: String)
    }

}