package com.kotdroid.demo.sample.views.utils

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Path
import android.graphics.PathMeasure
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.kotdroid.demo.sample.R

@TargetApi(23)
class HalfCurveLayoutManager internal constructor(private var context: Context, factor: Float) : CenterEdgeItemsRecyclerView.ChildLayoutManager(context, factor) {

    private val mCurvePath = Path()
    private val mPathMeasure = PathMeasure()
    private val mPathPoints = FloatArray(2)
    private val mPathTangent = FloatArray(2)
    private val mAnchorOffsetXY = FloatArray(2)
    private var mCurvePathHeight: Int = 0
    private var mPathLength: Float = 0.toFloat()
    private var mCurveBottom: Float = 0.toFloat()
    private var mCurveTop: Float = 0.toFloat()
    private var mLineGradient: Float = 0.toFloat()
    private var mParentView: RecyclerView? = null
    private var mLayoutWidth: Int = 0

    private var mLayoutHeight: Int = 0

    override fun updateChild(child: View?, parent: CenterEdgeItemsRecyclerView) {
        if (this.mParentView !== parent) {
            this.mParentView = parent
            this.mLayoutWidth = this.mParentView!!.width
            this.mLayoutHeight = this.mParentView!!.height
        }
        val childHeight = child!!.height
        val mParentViewHeight = this.mParentView!!.height

        val mXCurveOffset = (this.mLayoutWidth / 2 + this.mLayoutWidth / 4).toFloat()
        this.setUpCircularInitialLayout(this.mLayoutWidth * 2 - this.mLayoutWidth / 4, this.mLayoutHeight)
        this.mAnchorOffsetXY[0] = mXCurveOffset
        this.mAnchorOffsetXY[1] = childHeight.toFloat() / 2.0f
        val minCenter = -childHeight.toFloat() / 2.0f
        val maxCenter = this.mLayoutHeight.toFloat() + childHeight.toFloat() / 2.0f
        val range = maxCenter - minCenter
        val verticalAnchor = child.top.toFloat() + this.mAnchorOffsetXY[1]
        val mYScrollProgress = (verticalAnchor + Math.abs(minCenter)) / range
        this.mPathMeasure.getPosTan(mYScrollProgress * this.mPathLength, this.mPathPoints, this.mPathTangent)
        val topClusterRisk = Math.abs(this.mPathPoints[1] - this.mCurveBottom) < 0.001f && minCenter < this.mPathPoints[1]
        val bottomClusterRisk = Math.abs(this.mPathPoints[1] - this.mCurveTop) < 0.001f && maxCenter > this.mPathPoints[1]
        if (topClusterRisk || bottomClusterRisk) {
            this.mPathPoints[1] = verticalAnchor
            this.mPathPoints[0] = Math.abs(verticalAnchor) * this.mLineGradient
        }
        val newLeft = (this.mPathPoints[0] - this.mAnchorOffsetXY[0]).toInt()
        //offset the horizontal position by x
        child.offsetLeftAndRight(newLeft - child.left)
        val verticalTranslation = this.mPathPoints[1] - verticalAnchor
        child.translationY = verticalTranslation


        //turning off the animation of center item
//        val centerOffset = childHeight.toFloat() / 2.0f / mParentViewHeight.toFloat()
//        val yRelativeToCenterOffset = child.y / mParentViewHeight + centerOffset
//        if (yRelativeToCenterOffset > 0.49 && yRelativeToCenterOffset < 0.51) {
//            child.animate().scaleX(1.125f).scaleY(1.125f).translationX(-70f).setInterpolator(AccelerateDecelerateInterpolator()).setDuration(250).setStartDelay(50).start()
//        } else {
//            child.animate().scaleX(1f).scaleY(1f).translationX(1f).setInterpolator(AccelerateDecelerateInterpolator()).setDuration(250).setStartDelay(50).start()
//        }
    }

    private fun setUpCircularInitialLayout(width: Int, height: Int) {
        if (this.mCurvePathHeight != height) {
            this.mCurvePathHeight = height
            this.mCurveBottom = -0.048f * height.toFloat()
            this.mCurveTop = 1.048f * height.toFloat()
            this.mLineGradient = 10.416667f
            this.mCurvePath.reset()

            this.mCurvePath.moveTo(0.5f * width.toFloat(), this.mCurveBottom)

//            this.mCurvePath.lineTo(0.34F * width, 0.075F * height)

//            this.mCurvePath.cubicTo(0.22F * width, 0.17F * height,
//                    0.13F * width, 0.32F * height,
//                    0.13F * width, 0.5F * height)
            val radius = context.resources.getDimension(R.dimen.emoji_view_radius)
            this.mCurvePath.addCircle(0.8f * width.toFloat(), (mCurveTop + mCurveBottom) / 2, radius, Path.Direction.CCW)
//            this.mCurvePath.addCircle(0.5f * width.toFloat(), (mCurveTop + mCurveBottom) / 2, radius, Path.Direction.CCW)
            //            RectF rectF = new RectF(-width, 0, width, height);
            //            this.mCurvePath.arcTo(rectF, 0, 90, true);

//            this.mCurvePath.cubicTo(0.13F * width, 0.68F * height,
//                    0.22F * width, 0.83F * height,
//                    0.34F * width, 0.925F * height)

            this.mCurvePath.moveTo(0.5f * width.toFloat(), this.mCurveTop)

            this.mPathMeasure.setPath(this.mCurvePath, false)
            this.mPathLength = this.mPathMeasure.length
        }
    }
}