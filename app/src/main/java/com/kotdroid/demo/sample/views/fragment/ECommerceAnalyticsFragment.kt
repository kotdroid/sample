package com.kotdroid.demo.sample.views.fragment

import android.os.Bundle
import android.view.View
import com.google.android.gms.tagmanager.DataLayer
import com.google.android.gms.tagmanager.TagManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.FirebaseAnalytics.Param.*
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.viewmodels.BaseViewModel
import kotlinx.android.synthetic.main.fragment_ecommerce_analytics.*


class ECommerceAnalyticsFragment : BaseFragment(), View.OnClickListener {


    private var mFirebaseAnalytics: FirebaseAnalytics? = null

    override val layoutId: Int
        get() = R.layout.fragment_ecommerce_analytics

    override val isNavigationBarEnabled: Boolean
        get() = false
    override val viewModel: BaseViewModel?
        get() = null


    override fun init() {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(activityContext)

        // Modify the log level of the logger to print out not only
// warning and error messages, but also verbose, debug, info messages.
        TagManager.getInstance(activityContext).setVerboseLoggingEnabled(true)

        btnProductImpression.setOnClickListener(this)
        btnTagManagerTesting.setOnClickListener(this)
    }

    override fun observeData() {
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnProductImpression -> {
                addProductImpression()
            }
            R.id.btnTagManagerTesting -> {
                testGoogleTagManager()
            }
        }
    }


    private fun testGoogleTagManager() {

        var datalayer = TagManager.getInstance(activityContext).dataLayer
//        datalayer.push("openScreen", DataLayer.mapOf("screenName", "ECommerceAnalyticsTest"))


        datalayer.pushEvent("productClick",
                DataLayer.mapOf(
                        "ecommerce", DataLayer.mapOf(
                        "click", DataLayer.mapOf(
                        "actionField", DataLayer.mapOf(
                        "list", "Search Results"),                    // Optional list property.
                        "products", DataLayer.listOf(
                        DataLayer.mapOf(
                                "name", "Triblend Android T-Shirt",       // Name or ID is required.
                                "id", "12345",
                                "price", "15.25",
                                "brand", "Google",
                                "category", "Apparel",
                                "variant", "Gray"))))))
        datalayer = null
    }

    private fun addProductImpression() {
// Define products with relevant parameters

        // Define products with relevant parameters

        val product1 = Bundle()
        product1.putString(ITEM_ID, "sku1234")  // ITEM_ID or ITEM_NAME is required
        product1.putString(ITEM_NAME, "Donut Friday Scented T-Shirt")
        product1.putString(ITEM_CATEGORY, "Apparel/Men/Shirts")
        product1.putString(ITEM_VARIANT, "Blue")
        product1.putString(ITEM_BRAND, "Google")
        product1.putDouble(PRICE, 29.99)
        product1.putString(CURRENCY, "USD")
        product1.putLong(INDEX, 1)     // Position of the item in the list

        val product2 = Bundle()
        product2.putString(ITEM_ID, "sku5678")
        product2.putString(ITEM_NAME, "Android Workout Capris")
        product2.putString(ITEM_CATEGORY, "Apparel/Women/Pants")
        product2.putString(ITEM_VARIANT, "Black")
        product2.putString(ITEM_BRAND, "Google")
        product2.putDouble(PRICE, 39.99)
        product2.putString(CURRENCY, "USD")
        product2.putLong(INDEX, 2)

// Prepare ecommerce bundle

        val items = ArrayList<Bundle>()
        items.add(product1)
        items.add(product2)

        val ecommerceBundle = Bundle()
        ecommerceBundle.putParcelableArrayList("items", items)

// Set relevant bundle-level parameters

        ecommerceBundle.putString(ITEM_LIST, "Search Results") // List name

// Log view_search_results or view_item_list event with ecommerce bundle

        mFirebaseAnalytics?.logEvent(FirebaseAnalytics.Event.VIEW_SEARCH_RESULTS, ecommerceBundle)
    }
}