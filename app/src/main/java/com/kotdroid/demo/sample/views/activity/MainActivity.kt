package com.kotdroid.demo.sample.views.activity

import android.app.Dialog
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import com.kotdroid.demo.sample.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.custom_dialog_layout.view.*
import java.io.File
import java.util.*

class MainActivity : AppCompatActivity() {

    companion object {
        private const val CUSTOM_DIALOG_ID = 1

        private const val DISCOVER_DURATION = 300
        private const val REQUEST_BLU = 1
    }

    private var mDialog: Dialog? = null
    private var root: File? = null
    internal var fileroot: File? = null
    private var curFolder: File? = null
    private var dialogView: View? = null
    private val fileList = ArrayList<String>()
    private var btAdatper: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        //giving root path
        curFolder = File(Environment.getExternalStorageDirectory().absolutePath)
//        curFolder = root

        btnSelectFile.setOnClickListener {
            showDialog(CUSTOM_DIALOG_ID)
        }

        btnSendFile.setOnClickListener {
            sendViaBluetooth()
        }
    }

    override fun onCreateDialog(id: Int): Dialog {
        when (id) {
            CUSTOM_DIALOG_ID -> {
                dialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog_layout, null)
                mDialog = Dialog(this)
                mDialog?.setContentView(dialogView)
                mDialog?.setTitle("File Selector")
                mDialog?.setCancelable(true)
                mDialog?.setCanceledOnTouchOutside(true)

                //open directory
                dialogView?.btnUp?.setOnClickListener {
                    ListDir(File(Environment.getExternalStorageDirectory().absolutePath).parentFile)
                }

                dialogView?.dialogListView?.setOnItemClickListener { parent, view, position, id ->
                    val selected = File(fileList[position])
                    when {
                        selected.isDirectory -> ListDir(selected)
                        selected.isFile -> getSelectedFile(selected)
                        else -> dismissDialog(CUSTOM_DIALOG_ID)
                    }
                }
            }
        }
        return mDialog!!
    }

    override fun onPrepareDialog(id: Int, dialog: Dialog) {
        super.onPrepareDialog(id, dialog)
        when (id) {
            CUSTOM_DIALOG_ID -> ListDir(curFolder)
        }
    }

    private fun ListDir(f: File?) {
        dialogView?.btnUp?.isEnabled = f != root
        curFolder = f
        dialogView?.tvFolder?.text = f?.absolutePath
        etFilePath.setText(f?.absolutePath)
        val files = f!!.listFiles()
        fileList.clear()

        for (file in files) {
            fileList.add(file.path)
        }
        val directoryList = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, fileList)
        dialogView?.dialogListView?.setAdapter(directoryList)
    }

    //Method for send file via bluetooth------------------------------------------------------------
    fun sendViaBluetooth() {
        if (etFilePath != null) {
            if (btAdatper == null) {
                Toast.makeText(this, "Device not support bluetooth", Toast.LENGTH_LONG).show()
            } else {
                enableBluetooth()
            }
        } else {
            Toast.makeText(this, "Please select a file.", Toast.LENGTH_LONG).show()
        }
    }

    private fun enableBluetooth() {
        val discoveryIntent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
        discoveryIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, DISCOVER_DURATION)
        startActivityForResult(discoveryIntent, REQUEST_BLU)
    }


    private fun getSelectedFile(f: File) {
        etFilePath.setText(f.absolutePath)
        fileList.clear()
        dismissDialog(CUSTOM_DIALOG_ID)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == DISCOVER_DURATION && requestCode == REQUEST_BLU) {
            val i = Intent()
            i.action = Intent.ACTION_SEND
            i.type = "*/*"
            val file = File(etFilePath.text.toString())

            i.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file))

            val pm = packageManager
            val list = pm.queryIntentActivities(i, 0)
            if (list.size > 0) {
                var packageName: String? = null
                var className: String? = null
                var found = false

                for (info in list) {
                    packageName = info.activityInfo.packageName
                    if (packageName == "com.android.bluetooth") {
                        className = info.activityInfo.name
                        found = true
                        break
                    }
                }
                //CHECK BLUETOOTH available or not------------------------------------------------
                if (!found) {
                    Toast.makeText(this, "Bluetooth not been found", Toast.LENGTH_LONG).show()
                } else {
                    i.setClassName(packageName!!, className!!)
                    startActivity(i)
                }
            }
        } else {
            Toast.makeText(this, "Bluetooth is cancelled", Toast.LENGTH_LONG).show()
        }
    }
}
