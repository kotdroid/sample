package com.kotdroid.demo.sample.views.fragment.sensorsample

import android.annotation.SuppressLint
import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.viewmodels.BaseViewModel
import com.kotdroid.demo.sample.views.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_environment_sensor.*

class EnvironmentSensorFragment : BaseFragment(), SensorEventListener {

    lateinit var mSensorManager: SensorManager
    private var mPressure: Sensor? = null

    override val layoutId: Int
        get() = R.layout.fragment_environment_sensor

    override val isNavigationBarEnabled: Boolean
        get() = false

    override val viewModel: BaseViewModel?
        get() = null

    override fun init() {

        mSensorManager = activityContext.getSystemService(Context.SENSOR_SERVICE) as SensorManager

        mPressure = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE)

    }

    override fun onResume() {
        super.onResume()
        mSensorManager.registerListener(this, mPressure, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun observeData() {

    }

    //Sensor callbacks
    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

    }

    @SuppressLint("SetTextI18n")
    override fun onSensorChanged(event: SensorEvent?) {
        tvAirPressure.text = tvAirPressure.text.toString().trim() + event!!.values[0]
    }

    override fun onPause() {
        super.onPause()
        mSensorManager.unregisterListener(this)
    }
}