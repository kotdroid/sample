package com.kotdroid.demo.sample.views.fragment.camerasample

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.RectF
import android.graphics.SurfaceTexture
import android.hardware.camera2.*
import android.media.CamcorderProfile
import android.media.MediaRecorder
import android.os.CountDownTimer
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import android.util.Size
import android.util.SparseIntArray
import android.view.Surface
import android.view.TextureView
import android.view.View
import android.widget.Toast
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.utils.Constants
import com.kotdroid.demo.sample.utils.GeneralFunctions
import com.kotdroid.demo.sample.utils.MarshMallowPermissions
import com.kotdroid.demo.sample.views.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_video.*
import java.io.IOException
import java.util.*
import java.util.concurrent.Semaphore
import java.util.concurrent.TimeUnit

abstract class BaseVideoFragment : BaseFragment() {

    companion object {

        private const val TAG = "BaseVideoFragment"

        private const val DEFAULT_VIDEO_RECORDING_DURATION_IN_SECONDS = 60
        private const val DEFAULT_INTERVAL = 1000L

        private const val CAMERA_FACING_BACK = 0
        private const val CAMERA_FACING_FRONT = 1

        private const val SENSOR_ORIENTATION_DEFAULT_DEGREES = 90
        private const val SENSOR_ORIENTATION_INVERSE_DEGREES = 270

        private val DEFAULT_ORIENTATIONS = SparseIntArray().apply {
            append(Surface.ROTATION_0, 90)
            append(Surface.ROTATION_90, 0)
            append(Surface.ROTATION_180, 270)
            append(Surface.ROTATION_270, 180)
        }
        private val INVERSE_ORIENTATIONS = SparseIntArray().apply {
            append(Surface.ROTATION_0, 270)
            append(Surface.ROTATION_90, 180)
            append(Surface.ROTATION_180, 90)
            append(Surface.ROTATION_270, 0)
        }

        val LOW_QUALITY_VIDEO_RATE = CamcorderProfile.get(CamcorderProfile.QUALITY_480P).videoBitRate
        val LOW_QUALITY_AUDIO_RATE = CamcorderProfile.get(CamcorderProfile.QUALITY_CIF).audioBitRate

        val MEDIUM_QUALITY_VIDEO_RATE = CamcorderProfile.get(CamcorderProfile.QUALITY_720P).videoBitRate
        val MEDIUM_QUALITY_AUDIO_RATE = CamcorderProfile.get(CamcorderProfile.QUALITY_480P).audioBitRate

        val HIGH_QUALITY_VIDEO_RATE = CamcorderProfile.get(CamcorderProfile.QUALITY_1080P).videoBitRate
        val HIGH_QUALITY_AUDIO_RATE = CamcorderProfile.get(CamcorderProfile.QUALITY_720P).audioBitRate

    }

    /**
     * A [Semaphore] to prevent the app from exiting before closing the camera.
     */
    private val mCameraOpenCloseLock = Semaphore(1)

    private val mMarshMallowPermissions by lazy { MarshMallowPermissions(this) }

    private val mStateCallback = object : CameraDevice.StateCallback() {

        override fun onOpened(cameraDevice: CameraDevice) {
            mCameraDevice = cameraDevice
            startPreview()
            mCameraOpenCloseLock.release()
            if (null != previewTextureView) {
                configureTransform(previewTextureView.width, previewTextureView.height)
            }
        }

        override fun onDisconnected(cameraDevice: CameraDevice) {
            mCameraOpenCloseLock.release()
            cameraDevice.close()
            mCameraDevice = null
        }

        override fun onError(cameraDevice: CameraDevice, error: Int) {
            mCameraOpenCloseLock.release()
            cameraDevice.close()
            mCameraDevice = null
            val activity = activity
            activity?.finish()
        }
    }

    private val mSurfaceTextureListener = object : TextureView.SurfaceTextureListener {

        override fun onSurfaceTextureAvailable(surfaceTexture: SurfaceTexture,
                                               width: Int, height: Int) {
            openCamera(width, height)
        }

        override fun onSurfaceTextureSizeChanged(surfaceTexture: SurfaceTexture,
                                                 width: Int, height: Int) {
            configureTransform(width, height)
        }

        override fun onSurfaceTextureDestroyed(surfaceTexture: SurfaceTexture) = true
        override fun onSurfaceTextureUpdated(surfaceTexture: SurfaceTexture) {}
    }

    private val videoRecordingTimer = object : CountDownTimer(31000, DEFAULT_INTERVAL) {
        override fun onFinish() {
            stopRecordingVideo()
            tvVideoRecordingTimer.text = getString(R.string.video_recording_time_initial)
        }

        @SuppressLint("SetTextI18n")
        override fun onTick(millisUntilFinished: Long) {
            val seconds = millisUntilFinished / 1000
            if (seconds < 10)
                updateUi("00:0${millisUntilFinished / 1000}")
            else
                updateUi("00:${millisUntilFinished / 1000}")
        }
    }

    @SuppressLint("SetTextI18n")
    private val mRecordingTimerRunnable = object : Runnable {
        override fun run() {
            if (seconds == 0) {
                stopRecordingVideo()
                return
            }

            if (seconds < 10) updateUi("00:0$seconds")
            else updateUi("00:$seconds")

            seconds -= 1
            mRecordingTimerHandler.postDelayed(this, DEFAULT_INTERVAL)
        }
    }

    private var mPreviewSession: CameraCaptureSession? = null
    private var mPreviewBuilder: CaptureRequest.Builder? = null
    private var mCameraDevice: CameraDevice? = null
    private var mMediaRecorder: MediaRecorder? = null
    private var mBackgroundThread: HandlerThread? = null
    private var mBackgroundHandler: Handler? = null
    private var mPreviewSize: Size? = null
    private var mVideoSize: Size? = null
    private var mRecordingTimerHandler = Handler()
    private var mNextVideoAbsolutePath: String? = null
    private var cameraFacing = CAMERA_FACING_BACK
    private var seconds = DEFAULT_VIDEO_RECORDING_DURATION_IN_SECONDS
    private var sensorOrientation: Int? = null
    var videoBitRate = HIGH_QUALITY_VIDEO_RATE
    var audioBitRate = HIGH_QUALITY_AUDIO_RATE
    var isRecordingVideo: Boolean = false


    override fun init() {

        // Set full Screen Preview
        activity?.window?.decorView?.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN or
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE

        setData()
    }

    override fun onResume() {
        super.onResume()
        startBackgroundThread()
        if (previewTextureView!!.isAvailable) {
            openCamera(previewTextureView.width, previewTextureView.height)
        } else {
            previewTextureView.surfaceTextureListener = mSurfaceTextureListener
        }
    }

    override fun onPause() {
        closeCamera()
        stopBackgroundThread()
        super.onPause()
    }

    /**
     * Starts a background thread and its [Handler].
     */
    private fun startBackgroundThread() {
        mBackgroundThread = HandlerThread("CameraBackground")
        mBackgroundThread!!.start()
        mBackgroundHandler = Handler(mBackgroundThread!!.looper)
    }

    /**
     * Tries to open a [CameraDevice]. The result is listened by `mStateCallback`.
     */
    @SuppressLint("MissingPermission")
    fun openCamera(width: Int, height: Int) {
        if (!mMarshMallowPermissions.hasGroupPermissionGrantedForVideo()) {
            mMarshMallowPermissions.requestGroupPermissionForVideo()
            return
        }
        val activity = activity
        if (null == activity || activity.isFinishing) {
            return
        }
        val manager = activity.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            Log.d(TAG, "tryAcquire")
//            if (!mCameraOpenCloseLock.tryAcquire(3000, TimeUnit.MILLISECONDS)) {
//                throw RuntimeException("Time out waiting to lock camera opening.")
//            }
            val cameraId = manager.cameraIdList[cameraFacing]

            // Choose the sizes for camera preview and video recording
            val characteristics = manager.getCameraCharacteristics(cameraId)
            val map = characteristics
                    .get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
            sensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION)
            if (map == null) {
                throw RuntimeException("Cannot get available preview/video sizes")
            }
            mVideoSize = chooseVideoSize(map.getOutputSizes(MediaRecorder::class.java))
            mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture::class.java),
                    width, height, mVideoSize!!)

            val orientation = resources.configuration.orientation
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                previewTextureView!!.setAspectRatio(mPreviewSize!!.width, mPreviewSize!!.height)
            } else {
                previewTextureView!!.setAspectRatio(mPreviewSize!!.height, mPreviewSize!!.width)
            }
            configureTransform(width, height)
            mMediaRecorder = MediaRecorder()
            manager.openCamera(cameraId, mStateCallback, null)
        } catch (e: CameraAccessException) {
            Toast.makeText(activity, "Cannot access the camera.", Toast.LENGTH_SHORT).show()
            activity.finish()
        } catch (e: NullPointerException) {
            // Currently an NPE is thrown when the Camera2API is used but not supported on the
            // device this code runs.
        } catch (e: InterruptedException) {
            throw RuntimeException("Interrupted while trying to lock camera opening.")
        }

    }

    /**
     * Start the camera preview.
     */
    private fun startPreview() {
        if (null == mCameraDevice || !previewTextureView!!.isAvailable || null == mPreviewSize) {
            return
        }
        try {
            closePreviewSession()
            val texture = previewTextureView.surfaceTexture!!
            texture.setDefaultBufferSize(mPreviewSize!!.width, mPreviewSize!!.height)
            mPreviewBuilder = mCameraDevice!!.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)

            val previewSurface = Surface(texture)
            mPreviewBuilder!!.addTarget(previewSurface)

            mCameraDevice!!.createCaptureSession(listOf(previewSurface),
                    object : CameraCaptureSession.StateCallback() {

                        override fun onConfigured(session: CameraCaptureSession) {
                            mPreviewSession = session
                            updatePreview()
                        }

                        override fun onConfigureFailed(session: CameraCaptureSession) {
                            val activity = activity
                            if (null != activity) {
                                Toast.makeText(activity, "Failed", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }, mBackgroundHandler)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }

    }

    /**
     * Update the camera preview. [.startPreview] needs to be called in advance.
     */
    private fun updatePreview() {
        if (null == mCameraDevice) {
            return
        }
        try {
            mPreviewBuilder?.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO)
            val thread = HandlerThread("CameraPreview")
            thread.start()
            mPreviewSession!!.setRepeatingRequest(mPreviewBuilder!!.build(), null, mBackgroundHandler)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun closePreviewSession() {
        if (mPreviewSession != null) {
            mPreviewSession!!.close()
            mPreviewSession = null
        }
    }

    @Suppress("NAME_SHADOWING")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (MarshMallowPermissions.VIDEO_GROUP_PERMISSION_REQUEST_CODE == requestCode) {
            var permissions = 0
            for (i in grantResults) {
                if (PackageManager.PERMISSION_GRANTED == i) {
                    permissions++
                }
            }

            if (permissions < 3) {
                mMarshMallowPermissions.requestGroupPermissionForVideo()
            } else {
                onResume()
            }

        }
    }

    @Throws(IOException::class)
    private fun setUpMediaRecorder() {
        val activity = activity ?: return

        mMediaRecorder?.apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setVideoSource(MediaRecorder.VideoSource.SURFACE)
            setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
            setVideoEncodingBitRate(videoBitRate)
            setVideoFrameRate(30)
            setVideoSize(mVideoSize!!.width, mVideoSize!!.height)
            setVideoEncoder(MediaRecorder.VideoEncoder.H264)
            setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
            setAudioEncodingBitRate(audioBitRate)

            if (mNextVideoAbsolutePath == null || mNextVideoAbsolutePath!!.isEmpty()) {
                mNextVideoAbsolutePath = GeneralFunctions.setUpVideoFile(
                        Constants.LOCAL_STORAGE_BASE_PATH_FOR_USER_VIDEOS)?.absolutePath
            }
            setOutputFile(mNextVideoAbsolutePath)

            val rotation = activity.windowManager.defaultDisplay.rotation
            when (rotation) {
                SENSOR_ORIENTATION_DEFAULT_DEGREES -> setOrientationHint(DEFAULT_ORIENTATIONS.get(rotation))
                SENSOR_ORIENTATION_INVERSE_DEGREES -> setOrientationHint(INVERSE_ORIENTATIONS.get(rotation))
            }
            prepare()
        }
    }

    fun startRecordingVideo() {
        if (null == mCameraDevice || !previewTextureView!!.isAvailable || null == mPreviewSize) {
            return
        }
        try {
            closePreviewSession()
            setUpMediaRecorder()
            val texture = previewTextureView.surfaceTexture!!
            texture.setDefaultBufferSize(mPreviewSize!!.width, mPreviewSize!!.height)
            mPreviewBuilder = mCameraDevice!!.createCaptureRequest(CameraDevice.TEMPLATE_RECORD)
            val surfaces = ArrayList<Surface>()

            // Set up Surface for the camera preview
            val previewSurface = Surface(texture)
            surfaces.add(previewSurface)
            mPreviewBuilder!!.addTarget(previewSurface)

            // Set up Surface for the MediaRecorder
            val recorderSurface = mMediaRecorder!!.surface
            surfaces.add(recorderSurface)
            mPreviewBuilder!!.addTarget(recorderSurface)

            // Start a capture session
            // Once the session starts, we can update the UI and start recording
            mCameraDevice!!.createCaptureSession(surfaces, object : CameraCaptureSession.StateCallback() {

                override fun onConfigured(cameraCaptureSession: CameraCaptureSession) {
                    mPreviewSession = cameraCaptureSession
                    updatePreview()
                    activity!!.runOnUiThread {
                        // UI
                        isRecordingVideo = true
                        tvVideoRecordingTimer.setTextColor(Color.RED)

                        // Start recording
                        mMediaRecorder!!.start()

                        // Start Chronometer
                        mRecordingTimerHandler.post(mRecordingTimerRunnable)

                    }
                }

                override fun onConfigureFailed(cameraCaptureSession: CameraCaptureSession) {
                    val activity = activity
                    if (null != activity) {
                        Toast.makeText(activity, "Failed", Toast.LENGTH_SHORT).show()
                    }
                }
            }, mBackgroundHandler)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    fun stopRecordingVideo() {
        // UI
        isRecordingVideo = false
        updateUi(getString(R.string.video_recording_time_initial))
        tvVideoRecordingTimer.setTextColor(Color.WHITE)

        // Stop Timer
        mRecordingTimerHandler.removeCallbacks(mRecordingTimerRunnable)

        // Stop recording
        mMediaRecorder!!.stop()
        mMediaRecorder!!.reset()

        val activity = activity
        if (null != activity) {
            Toast.makeText(activity, "Video saved: " + mNextVideoAbsolutePath!!,
                    Toast.LENGTH_SHORT).show()
            Log.d(TAG, "Video saved: " + mNextVideoAbsolutePath!!)
        }
        mNextVideoAbsolutePath = null
        startPreview()
    }

    /**
     * Stops the background thread and its [Handler].
     */
    private fun stopBackgroundThread() {
        try {
            mBackgroundThread!!.quitSafely()
            mBackgroundThread!!.join()
            mBackgroundThread = null
            mBackgroundHandler = null
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Switch between front and back cameraDevice
     */
    fun toggleCamera() {
        cameraFacing = if (CAMERA_FACING_BACK == cameraFacing) CAMERA_FACING_FRONT
        else CAMERA_FACING_BACK
        closeCamera()

        if (previewTextureView.isAvailable) openCamera(previewTextureView.width, previewTextureView.height)
        else previewTextureView.surfaceTextureListener = mSurfaceTextureListener
    }

    private fun closeCamera() {
        try {
            mCameraOpenCloseLock.acquire()
            closePreviewSession()
            if (null != mCameraDevice) {
                mCameraDevice!!.close()
                mCameraDevice = null
            }
            if (null != mMediaRecorder) {
                mMediaRecorder!!.release()
                mMediaRecorder = null
            }
        } catch (e: InterruptedException) {
            throw RuntimeException("Interrupted while trying to lock camera closing.")
        } finally {
            mCameraOpenCloseLock.release()
        }
    }

    /**
     * Configures the necessary [android.graphics.Matrix] transformation to `previewTextureView`.
     * This method should not to be called until the camera preview size is determined in
     * openCamera, or until the size of `previewTextureView` is fixed.
     *
     * @param viewWidth  The width of `previewTextureView`
     * @param viewHeight The height of `previewTextureView`
     */
    private fun configureTransform(viewWidth: Int, viewHeight: Int) {
        val activity = activity
        if (null == previewTextureView || null == mPreviewSize || null == activity) {
            return
        }
        val rotation = activity.windowManager.defaultDisplay.rotation
        val matrix = Matrix()
        val viewRect = RectF(0f, 0f, viewWidth.toFloat(), viewHeight.toFloat())
        val bufferRect = RectF(0f, 0f, mPreviewSize!!.height.toFloat(), mPreviewSize!!.width.toFloat())
        val centerX = viewRect.centerX()
        val centerY = viewRect.centerY()
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY())
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL)
            val scale = Math.max(
                    viewHeight.toFloat() / mPreviewSize!!.height,
                    viewWidth.toFloat() / mPreviewSize!!.width)
            matrix.postScale(scale, scale, centerX, centerY)
            matrix.postRotate((90 * (rotation - 2)).toFloat(), centerX, centerY)
        }
        previewTextureView.setTransform(matrix)
    }

    /**
     * Compares two `Size`s based on their areas.
     */
    internal class CompareSizesByArea : Comparator<Size> {
        override fun compare(lhs: Size, rhs: Size): Int {
            // We cast here to ensure the multiplications won't overflow
            return java.lang.Long.signum(lhs.width.toLong() * lhs.height - rhs.width.toLong() * rhs.height)
        }
    }

    /**
     * In this sample, we choose a video size with 3x4 aspect ratio. Also, we don't use sizes
     * larger than 1080p, since MediaRecorder cannot handle such a high-resolution video.
     *
     * @param choices The list of available sizes
     * @return The video size
     */
    private fun chooseVideoSize(choices: Array<Size>): Size {
        for (size in choices) {
            if (size.width == size.height * 16 / 9 && size.width <= 1080) {
                return size
            }
        }
        Log.e(TAG, "Couldn't find any suitable video size")
        return choices[choices.size - 1]
    }

    /**
     * Given `choices` of `Size`s supported by a camera, chooses the smallest one whose
     * width and height are at least as large as the respective requested values, and whose aspect
     * ratio matches with the specified value.
     *
     * @param choices     The list of sizes that the camera supports for the intended output class
     * @param width       The minimum desired width
     * @param height      The minimum desired height
     * @param aspectRatio The aspect ratio
     * @return The optimal `Size`, or an arbitrary one if none were big enough
     */
    private fun chooseOptimalSize(choices: Array<Size>, width: Int, height: Int, aspectRatio: Size): Size {
        // Collect the supported resolutions that are at least as big as the preview Surface
        val bigEnough = ArrayList<Size>()
        val w = aspectRatio.width
        val h = aspectRatio.height
        for (option in choices) {
            if (option.height == option.width * h / w &&
                    option.width >= width && option.height >= height) {
                bigEnough.add(option)
            }
        }

        // Pick the smallest of those, assuming we found any
        return if (bigEnough.size > 0) {
            Collections.min(bigEnough, CompareSizesByArea())
        } else {
            choices[0]
        }
    }

    abstract fun updateUi(time: String)
    abstract fun setData()
}