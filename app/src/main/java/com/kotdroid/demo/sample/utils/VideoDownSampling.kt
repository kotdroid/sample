//package com.kotdroid.demo.sample.utils
//
//
//import android.content.Context
//import android.graphics.Bitmap
//import android.media.MediaCodec
//import android.media.MediaCodecInfo
//import android.media.MediaCodecList
//import android.media.MediaExtractor
//import android.media.MediaFormat
//import android.media.MediaMetadataRetriever
//import android.media.MediaMuxer
//import android.view.Surface
//
//import java.io.File
//import java.io.IOException
//import java.nio.ByteBuffer
//import java.util.concurrent.ConcurrentLinkedQueue
//import java.util.concurrent.atomic.AtomicReference
//
//
//class VideoDownsampling(context: Context) {
//
//    private var mWidth = 1280
//    private var mHeight = 720
//
//    private inner class VideoUpload(internal var original: String, internal var outFile: String?, internal var sizeInputFile: Long) {
//        internal var percentage: Int = 0
//        internal var sizeRead: Long = 0
//
//        init {
//            this.percentage = 0
//            this.sizeRead = 0
//        }
//    }
//
//    init {
//        this.context = context
//        queue = ConcurrentLinkedQueue()
//    }
//
//    @Throws(Throwable::class)
//    fun changeResolution(f: File, inputFile: String) {
//        log("changeResolution")
//
//        queue.add(VideoUpload(f.absolutePath, inputFile, f.length()))
//
//        ChangerWrapper.changeResolutionInSeparatedThread(this)
//    }
//
//    private class ChangerWrapper private constructor(private val mChanger: VideoDownsampling) : Runnable {
//
//        private var mThrowable: Throwable? = null
//
//        override fun run() {
//            val video = queue.peek()
//
//            val out = video.outFile
//
//            try {
//                while (!queue.isEmpty()) {
//                    mChanger.prepareAndChangeResolution()
//                }
//            } catch (th: Throwable) {
//                mThrowable = th
//                if (out != null) {
//                    (context as ChatUploadService).finishDownsampling(out, false)
//                }
//            }
//
//        }
//
//        companion object {
//
//            @Throws(Throwable::class)
//            fun changeResolutionInSeparatedThread(changer: VideoDownsampling) {
//                log("changeResolutionInSeparatedThread")
//                val wrapper = ChangerWrapper(changer)
//                val th = Thread(wrapper, ChangerWrapper::class.java.simpleName)
//                th.start()
//                //            th.join();
//                if (wrapper.mThrowable != null)
//                    throw wrapper.mThrowable
//            }
//        }
//    }
//
//    @Throws(Exception::class)
//    private fun prepareAndChangeResolution() {
//        log("prepareAndChangeResolution")
//        var exception: Exception? = null
//
//        val video = queue.poll()
//
//        val mInputFile = video.original
//
//        val mOutputFile = video.outFile
//
//        val videoCodecInfo = selectCodec(OUTPUT_VIDEO_MIME_TYPE) ?: return
//        val audioCodecInfo = selectCodec(OUTPUT_AUDIO_MIME_TYPE) ?: return
//
//        var videoExtractor: MediaExtractor? = null
//        var audioExtractor: MediaExtractor? = null
//        var outputSurface: OutputSurface? = null
//        var videoDecoder: MediaCodec? = null
//        var audioDecoder: MediaCodec? = null
//        var videoEncoder: MediaCodec? = null
//        var audioEncoder: MediaCodec? = null
//        var muxer: MediaMuxer? = null
//        var inputSurface: InputSurface? = null
//        try {
//            videoExtractor = createExtractor(mInputFile)
//            val videoInputTrack = getAndSelectVideoTrackIndex(videoExtractor)
//            val inputFormat = videoExtractor.getTrackFormat(videoInputTrack)
//
//            val m = MediaMetadataRetriever()
//            m.setDataSource(mInputFile)
//            val thumbnail = m.frameAtTime
//            val inputWidth = thumbnail.width
//            val inputHeight = thumbnail.height
//
//            if (inputWidth > inputHeight) {
//                if (mWidth < mHeight) {
//                    val w = mWidth
//                    mWidth = mHeight
//                    mHeight = w
//                }
//            } else {
//                if (mWidth > mHeight) {
//                    val w = mWidth
//                    mWidth = mHeight
//                    mHeight = w
//                }
//            }
//
//            val outputVideoFormat = MediaFormat.createVideoFormat(OUTPUT_VIDEO_MIME_TYPE, mWidth, mHeight)
//            outputVideoFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT, OUTPUT_VIDEO_COLOR_FORMAT)
//            outputVideoFormat.setInteger(MediaFormat.KEY_BIT_RATE, OUTPUT_VIDEO_BIT_RATE)
//            outputVideoFormat.setInteger(MediaFormat.KEY_FRAME_RATE, OUTPUT_VIDEO_FRAME_RATE)
//            outputVideoFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, OUTPUT_VIDEO_IFRAME_INTERVAL)
//
//            val inputSurfaceReference = AtomicReference<Surface>()
//            videoEncoder = createVideoEncoder(videoCodecInfo, outputVideoFormat, inputSurfaceReference)
//            inputSurface = InputSurface(inputSurfaceReference.get())
//            inputSurface!!.makeCurrent()
//
//            outputSurface = OutputSurface()
//            videoDecoder = createVideoDecoder(inputFormat, outputSurface!!.getSurface())
//
//            audioExtractor = createExtractor(mInputFile)
//            val audioInputTrack = getAndSelectAudioTrackIndex(audioExtractor)
//            val inputAudioFormat = audioExtractor.getTrackFormat(audioInputTrack)
//            val outputAudioFormat = MediaFormat.createAudioFormat(inputAudioFormat.getString(MediaFormat.KEY_MIME),
//                    inputAudioFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE),
//                    inputAudioFormat.getInteger(MediaFormat.KEY_CHANNEL_COUNT))
//            outputAudioFormat.setInteger(MediaFormat.KEY_BIT_RATE, OUTPUT_AUDIO_BIT_RATE)
//            outputAudioFormat.setInteger(MediaFormat.KEY_AAC_PROFILE, OUTPUT_AUDIO_AAC_PROFILE)
//
//            audioEncoder = createAudioEncoder(audioCodecInfo, outputAudioFormat)
//            audioDecoder = createAudioDecoder(inputAudioFormat)
//
//            muxer = MediaMuxer(mOutputFile!!, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4)
//
//            changeResolution(videoExtractor, audioExtractor, videoDecoder, videoEncoder, audioDecoder, audioEncoder, muxer, inputSurface, outputSurface, video)
//        } finally {
//            try {
//                videoExtractor?.release()
//            } catch (e: Exception) {
//                if (exception == null)
//                    exception = e
//            }
//
//            try {
//                audioExtractor?.release()
//            } catch (e: Exception) {
//                if (exception == null)
//                    exception = e
//            }
//
//            try {
//                if (videoDecoder != null) {
//                    videoDecoder.stop()
//                    videoDecoder.release()
//                }
//            } catch (e: Exception) {
//                if (exception == null)
//                    exception = e
//            }
//
//            try {
//                if (outputSurface != null) {
//                    outputSurface!!.release()
//                }
//            } catch (e: Exception) {
//                if (exception == null)
//                    exception = e
//            }
//
//            try {
//                if (videoEncoder != null) {
//                    videoEncoder.stop()
//                    videoEncoder.release()
//                }
//            } catch (e: Exception) {
//                if (exception == null)
//                    exception = e
//            }
//
//            try {
//                if (audioDecoder != null) {
//                    audioDecoder.stop()
//                    audioDecoder.release()
//                }
//            } catch (e: Exception) {
//                if (exception == null)
//                    exception = e
//            }
//
//            try {
//                if (audioEncoder != null) {
//                    audioEncoder.stop()
//                    audioEncoder.release()
//                }
//            } catch (e: Exception) {
//                if (exception == null)
//                    exception = e
//            }
//
//            try {
//                if (muxer != null) {
//                    muxer.stop()
//                    muxer.release()
//                }
//            } catch (e: Exception) {
//                if (exception == null)
//                    exception = e
//            }
//
//            try {
//                if (inputSurface != null)
//                    inputSurface!!.release()
//            } catch (e: Exception) {
//                if (exception == null)
//                    exception = e
//            }
//
//        }
//        if (exception != null) {
//            log("VideoDownsampling Exception: " + exception.toString())
//            throw exception
//        } else {
//            (context as ChatUploadService).finishDownsampling(mOutputFile, true)
//        }
//    }
//
//    @Throws(IOException::class)
//    private fun createExtractor(mInputFile: String): MediaExtractor {
//        val extractor: MediaExtractor
//        extractor = MediaExtractor()
//        extractor.setDataSource(mInputFile)
//        return extractor
//    }
//
//    @Throws(IOException::class)
//    private fun createVideoDecoder(inputFormat: MediaFormat, surface: Surface): MediaCodec {
//        val decoder = MediaCodec.createDecoderByType(getMimeTypeFor(inputFormat))
//        decoder.configure(inputFormat, surface, null, 0)
//        decoder.start()
//        return decoder
//    }
//
//    @Throws(IOException::class)
//    private fun createVideoEncoder(codecInfo: MediaCodecInfo, format: MediaFormat, surfaceReference: AtomicReference<Surface>): MediaCodec {
//        val encoder = MediaCodec.createByCodecName(codecInfo.name)
//        encoder.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE)
//        surfaceReference.set(encoder.createInputSurface())
//        encoder.start()
//        return encoder
//    }
//
//    @Throws(IOException::class)
//    private fun createAudioDecoder(inputFormat: MediaFormat): MediaCodec {
//        val decoder = MediaCodec.createDecoderByType(getMimeTypeFor(inputFormat))
//        decoder.configure(inputFormat, null, null, 0)
//        decoder.start()
//        return decoder
//    }
//
//    @Throws(IOException::class)
//    private fun createAudioEncoder(codecInfo: MediaCodecInfo, format: MediaFormat): MediaCodec {
//        val encoder = MediaCodec.createByCodecName(codecInfo.name)
//        encoder.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE)
//        encoder.start()
//        return encoder
//    }
//
//    private fun getAndSelectVideoTrackIndex(extractor: MediaExtractor): Int {
//        for (index in 0 until extractor.trackCount) {
//            if (isVideoFormat(extractor.getTrackFormat(index))) {
//                extractor.selectTrack(index)
//                return index
//            }
//        }
//        return -1
//    }
//
//    private fun getAndSelectAudioTrackIndex(extractor: MediaExtractor): Int {
//        for (index in 0 until extractor.trackCount) {
//            if (isAudioFormat(extractor.getTrackFormat(index))) {
//                extractor.selectTrack(index)
//                return index
//            }
//        }
//        return -1
//    }
//
//    private fun changeResolution(videoExtractor: MediaExtractor?, audioExtractor: MediaExtractor?,
//                                 videoDecoder: MediaCodec, videoEncoder: MediaCodec,
//                                 audioDecoder: MediaCodec, audioEncoder: MediaCodec,
//                                 muxer: MediaMuxer?,
//                                 inputSurface: InputSurface?, outputSurface: OutputSurface?, video: VideoUpload) {
//        log("changeResolution")
//        val mOutputFile = video.outFile
//
//        var videoDecoderInputBuffers: Array<ByteBuffer>? = null
//        var videoDecoderOutputBuffers: Array<ByteBuffer>? = null
//        var videoEncoderOutputBuffers: Array<ByteBuffer>? = null
//        var videoDecoderOutputBufferInfo: MediaCodec.BufferInfo? = null
//        var videoEncoderOutputBufferInfo: MediaCodec.BufferInfo? = null
//
//        videoDecoderInputBuffers = videoDecoder.inputBuffers
//        videoDecoderOutputBuffers = videoDecoder.outputBuffers
//        videoEncoderOutputBuffers = videoEncoder.outputBuffers
//        videoDecoderOutputBufferInfo = MediaCodec.BufferInfo()
//        videoEncoderOutputBufferInfo = MediaCodec.BufferInfo()
//
//        var audioDecoderInputBuffers: Array<ByteBuffer>? = null
//        var audioDecoderOutputBuffers: Array<ByteBuffer>? = null
//        var audioEncoderInputBuffers: Array<ByteBuffer>? = null
//        var audioEncoderOutputBuffers: Array<ByteBuffer>? = null
//        var audioDecoderOutputBufferInfo: MediaCodec.BufferInfo? = null
//        var audioEncoderOutputBufferInfo: MediaCodec.BufferInfo? = null
//
//        audioDecoderInputBuffers = audioDecoder.inputBuffers
//        audioDecoderOutputBuffers = audioDecoder.outputBuffers
//        audioEncoderInputBuffers = audioEncoder.inputBuffers
//        audioEncoderOutputBuffers = audioEncoder.outputBuffers
//        audioDecoderOutputBufferInfo = MediaCodec.BufferInfo()
//        audioEncoderOutputBufferInfo = MediaCodec.BufferInfo()
//
//        var decoderOutputVideoFormat: MediaFormat? = null
//        var decoderOutputAudioFormat: MediaFormat? = null
//        var encoderOutputVideoFormat: MediaFormat? = null
//        var encoderOutputAudioFormat: MediaFormat? = null
//        var outputVideoTrack = -1
//        var outputAudioTrack = -1
//
//        var videoExtractorDone = false
//        var videoDecoderDone = false
//        var videoEncoderDone = false
//
//        var audioExtractorDone = false
//        var audioDecoderDone = false
//        var audioEncoderDone = false
//
//        var pendingAudioDecoderOutputBufferIndex = -1
//        var muxing = false
//        while (!videoEncoderDone || !audioEncoderDone) {
//            while (!videoExtractorDone && (encoderOutputVideoFormat == null || muxing)) {
//                val decoderInputBufferIndex = videoDecoder.dequeueInputBuffer(TIMEOUT_USEC.toLong())
//                if (decoderInputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER)
//                    break
//
//                val decoderInputBuffer = videoDecoderInputBuffers!![decoderInputBufferIndex]
//                val size = videoExtractor!!.readSampleData(decoderInputBuffer, 0)
//                val presentationTime = videoExtractor.sampleTime
//                video.sizeRead += size.toLong()
//
//                if (size >= 0) {
//                    videoDecoder.queueInputBuffer(decoderInputBufferIndex, 0, size, presentationTime, videoExtractor.sampleFlags)
//                }
//                videoExtractorDone = !videoExtractor.advance()
//                if (videoExtractorDone)
//                    videoDecoder.queueInputBuffer(decoderInputBufferIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM)
//                break
//            }
//
//            while (!audioExtractorDone && (encoderOutputAudioFormat == null || muxing)) {
//                val decoderInputBufferIndex = audioDecoder.dequeueInputBuffer(TIMEOUT_USEC.toLong())
//                if (decoderInputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER)
//                    break
//
//                val decoderInputBuffer = audioDecoderInputBuffers!![decoderInputBufferIndex]
//                val size = audioExtractor!!.readSampleData(decoderInputBuffer, 0)
//                val presentationTime = audioExtractor.sampleTime
//                video.sizeRead += size.toLong()
//
//                if (size >= 0)
//                    audioDecoder.queueInputBuffer(decoderInputBufferIndex, 0, size, presentationTime, audioExtractor.sampleFlags)
//
//                audioExtractorDone = !audioExtractor.advance()
//                if (audioExtractorDone)
//                    audioDecoder.queueInputBuffer(decoderInputBufferIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM)
//
//                break
//            }
//
//            while (!videoDecoderDone && (encoderOutputVideoFormat == null || muxing)) {
//                val decoderOutputBufferIndex = videoDecoder.dequeueOutputBuffer(videoDecoderOutputBufferInfo, TIMEOUT_USEC.toLong())
//                if (decoderOutputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER)
//                    break
//
//                if (decoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
//                    videoDecoderOutputBuffers = videoDecoder.outputBuffers
//                    break
//                }
//                if (decoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
//                    decoderOutputVideoFormat = videoDecoder.outputFormat
//                    break
//                }
//
//                val decoderOutputBuffer = videoDecoderOutputBuffers!![decoderOutputBufferIndex]
//                if (videoDecoderOutputBufferInfo.flags and MediaCodec.BUFFER_FLAG_CODEC_CONFIG != 0) {
//                    videoDecoder.releaseOutputBuffer(decoderOutputBufferIndex, false)
//                    break
//                }
//
//                val render = videoDecoderOutputBufferInfo.size != 0
//                videoDecoder.releaseOutputBuffer(decoderOutputBufferIndex, render)
//                if (render) {
//                    outputSurface!!.awaitNewImage()
//                    outputSurface!!.drawImage()
//                    inputSurface!!.setPresentationTime(videoDecoderOutputBufferInfo.presentationTimeUs * 1000)
//                    inputSurface!!.swapBuffers()
//                }
//                if (videoDecoderOutputBufferInfo.flags and MediaCodec.BUFFER_FLAG_END_OF_STREAM != 0) {
//                    videoDecoderDone = true
//                    videoEncoder.signalEndOfInputStream()
//                }
//                break
//            }
//
//            while (!audioDecoderDone && pendingAudioDecoderOutputBufferIndex == -1 && (encoderOutputAudioFormat == null || muxing)) {
//                val decoderOutputBufferIndex = audioDecoder.dequeueOutputBuffer(audioDecoderOutputBufferInfo, TIMEOUT_USEC.toLong())
//                if (decoderOutputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER)
//                    break
//
//                if (decoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
//                    audioDecoderOutputBuffers = audioDecoder.outputBuffers
//                    break
//                }
//                if (decoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
//                    decoderOutputAudioFormat = audioDecoder.outputFormat
//                    break
//                }
//                val decoderOutputBuffer = audioDecoderOutputBuffers!![decoderOutputBufferIndex]
//                if (audioDecoderOutputBufferInfo.flags and MediaCodec.BUFFER_FLAG_CODEC_CONFIG != 0) {
//                    audioDecoder.releaseOutputBuffer(decoderOutputBufferIndex, false)
//                    break
//                }
//                pendingAudioDecoderOutputBufferIndex = decoderOutputBufferIndex
//                break
//            }
//
//            while (pendingAudioDecoderOutputBufferIndex != -1) {
//                val encoderInputBufferIndex = audioEncoder.dequeueInputBuffer(TIMEOUT_USEC.toLong())
//                val encoderInputBuffer = audioEncoderInputBuffers!![encoderInputBufferIndex]
//                val size = audioDecoderOutputBufferInfo.size
//                val presentationTime = audioDecoderOutputBufferInfo.presentationTimeUs
//
//                if (size >= 0) {
//                    val decoderOutputBuffer = audioDecoderOutputBuffers!![pendingAudioDecoderOutputBufferIndex].duplicate()
//                    decoderOutputBuffer.position(audioDecoderOutputBufferInfo.offset)
//                    decoderOutputBuffer.limit(audioDecoderOutputBufferInfo.offset + size)
//                    encoderInputBuffer.position(0)
//                    encoderInputBuffer.put(decoderOutputBuffer)
//                    audioEncoder.queueInputBuffer(encoderInputBufferIndex, 0, size, presentationTime, audioDecoderOutputBufferInfo.flags)
//                }
//                audioDecoder.releaseOutputBuffer(pendingAudioDecoderOutputBufferIndex, false)
//                pendingAudioDecoderOutputBufferIndex = -1
//                if (audioDecoderOutputBufferInfo.flags and MediaCodec.BUFFER_FLAG_END_OF_STREAM != 0)
//                    audioDecoderDone = true
//
//                break
//            }
//
//            while (!videoEncoderDone && (encoderOutputVideoFormat == null || muxing)) {
//                val encoderOutputBufferIndex = videoEncoder.dequeueOutputBuffer(videoEncoderOutputBufferInfo, TIMEOUT_USEC.toLong())
//                if (encoderOutputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER)
//                    break
//                if (encoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
//                    videoEncoderOutputBuffers = videoEncoder.outputBuffers
//                    break
//                }
//                if (encoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
//                    encoderOutputVideoFormat = videoEncoder.outputFormat
//                    break
//                }
//
//                val encoderOutputBuffer = videoEncoderOutputBuffers!![encoderOutputBufferIndex]
//                if (videoEncoderOutputBufferInfo.flags and MediaCodec.BUFFER_FLAG_CODEC_CONFIG != 0) {
//                    videoEncoder.releaseOutputBuffer(encoderOutputBufferIndex, false)
//                    break
//                }
//                if (videoEncoderOutputBufferInfo.size != 0) {
//                    muxer!!.writeSampleData(outputVideoTrack, encoderOutputBuffer, videoEncoderOutputBufferInfo)
//                }
//                if (videoEncoderOutputBufferInfo.flags and MediaCodec.BUFFER_FLAG_END_OF_STREAM != 0) {
//                    videoEncoderDone = true
//                }
//                videoEncoder.releaseOutputBuffer(encoderOutputBufferIndex, false)
//                break
//            }
//
//            while (!audioEncoderDone && (encoderOutputAudioFormat == null || muxing)) {
//                val encoderOutputBufferIndex = audioEncoder.dequeueOutputBuffer(audioEncoderOutputBufferInfo, TIMEOUT_USEC.toLong())
//                if (encoderOutputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
//                    break
//                }
//                if (encoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
//                    audioEncoderOutputBuffers = audioEncoder.outputBuffers
//                    break
//                }
//                if (encoderOutputBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
//                    encoderOutputAudioFormat = audioEncoder.outputFormat
//                    break
//                }
//
//                val encoderOutputBuffer = audioEncoderOutputBuffers!![encoderOutputBufferIndex]
//                if (audioEncoderOutputBufferInfo.flags and MediaCodec.BUFFER_FLAG_CODEC_CONFIG != 0) {
//                    audioEncoder.releaseOutputBuffer(encoderOutputBufferIndex, false)
//                    break
//                }
//                if (audioEncoderOutputBufferInfo.size != 0)
//                    muxer!!.writeSampleData(outputAudioTrack, encoderOutputBuffer, audioEncoderOutputBufferInfo)
//                if (audioEncoderOutputBufferInfo.flags and MediaCodec.BUFFER_FLAG_END_OF_STREAM != 0)
//                    audioEncoderDone = true
//
//                audioEncoder.releaseOutputBuffer(encoderOutputBufferIndex, false)
//                break
//            }
//            if (!muxing && encoderOutputAudioFormat != null && encoderOutputVideoFormat != null) {
//                outputVideoTrack = muxer!!.addTrack(encoderOutputVideoFormat)
//                outputAudioTrack = muxer.addTrack(encoderOutputAudioFormat)
//                muxer.start()
//                muxing = true
//            }
//
//            video.percentage = (100 * video.sizeRead / video.sizeInputFile).toInt()
//            //            log("The percentage complete is: " + video.percentage + " (" + video.sizeRead + "/" + video.sizeInputFile +")");
//            if (video.percentage % 5 == 0) {
//                //                log("Percentage: "+mOutputFile + "  " + video.percentage + " (" + video.sizeInputFile + "/" + video.sizeInputFile +")");
//                (context as ChatUploadService).updateProgressDownsampling(video.percentage, mOutputFile)
//            }
//        }
//        video.percentage = 100
//    }
//
//    companion object {
//
//        private val TIMEOUT_USEC = 10000
//
//        private val OUTPUT_VIDEO_MIME_TYPE = "video/avc"
//        private val OUTPUT_VIDEO_BIT_RATE = 1280 * 720
//        private val OUTPUT_VIDEO_FRAME_RATE = 30
//        private val OUTPUT_VIDEO_IFRAME_INTERVAL = 10
//        private val OUTPUT_VIDEO_COLOR_FORMAT = MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface
//
//        private val OUTPUT_AUDIO_MIME_TYPE = "audio/mp4a-latm"
//        private val OUTPUT_AUDIO_CHANNEL_COUNT = 2
//        private val OUTPUT_AUDIO_BIT_RATE = 128 * 1024
//        private val OUTPUT_AUDIO_AAC_PROFILE = MediaCodecInfo.CodecProfileLevel.AACObjectHE
//        private val OUTPUT_AUDIO_SAMPLE_RATE_HZ = 44100
//
//        internal var context: Context
//
//        internal var queue: ConcurrentLinkedQueue<VideoUpload>
//
//        private fun isVideoFormat(format: MediaFormat): Boolean {
//            return getMimeTypeFor(format).startsWith("video/")
//        }
//
//        private fun isAudioFormat(format: MediaFormat): Boolean {
//            return getMimeTypeFor(format).startsWith("audio/")
//        }
//
//        private fun getMimeTypeFor(format: MediaFormat): String {
//            return format.getString(MediaFormat.KEY_MIME)
//        }
//
//        private fun selectCodec(mimeType: String): MediaCodecInfo? {
//            val numCodecs = MediaCodecList.getCodecCount()
//            for (i in 0 until numCodecs) {
//                val codecInfo = MediaCodecList.getCodecInfoAt(i)
//                if (!codecInfo.isEncoder) {
//                    continue
//                }
//                val types = codecInfo.supportedTypes
//                for (j in types.indices) {
//                    if (types[j].equals(mimeType, ignoreCase = true)) {
//                        return codecInfo
//                    }
//                }
//            }
//            return null
//        }
//
//        private fun log(log: String) {
////            Util.log("VideoDownsampling", log)
//        }
//    }
//}