package com.kotdroid.demo.sample.views.fragment.locationsample

import android.annotation.SuppressLint
import androidx.fragment.app.Fragment
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.kotdroid.demo.sample.R


abstract class BaseMapFragment : BaseLocationFragment(), OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener {

    companion object {
        const val MY_LOCATION = R.drawable.current_location
        const val ZOOM_FACTOR: Float = 12f
        const val REQUEST_LOCATION = 485
    }

    //map related variables
    private var mapFragment: Any? = null
    var mGoogleMap: GoogleMap? = null
    var areaOverLay: Circle? = null
    var mapCenterLatLng: LatLng? = null


    override fun setData() {

        //checking if google play services available in device
        if (isPlayServicesAvailable()) {
            if (null == mapFragment) {
                mapFragment = SupportMapFragment.newInstance()
                (mapFragment as SupportMapFragment).getMapAsync(this)
            }
            childFragmentManager.beginTransaction().replace(R.id.flMap, mapFragment as Fragment)
        }

        setMapData()
    }


    private fun isPlayServicesAvailable(): Boolean {

        val apiAvailability = GoogleApiAvailability.getInstance()
        val isAvailable = apiAvailability.isGooglePlayServicesAvailable(activityContext)
        when {
            isAvailable == ConnectionResult.SUCCESS -> return true
            apiAvailability.isUserResolvableError(isAvailable) -> apiAvailability.getErrorDialog(activity, isAvailable, 0).show()
            else -> showMessage()
        }

        return false
    }


    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap?) {

        //customizing map
        mGoogleMap = googleMap
        mGoogleMap?.mapType = GoogleMap.MAP_TYPE_TERRAIN
        mGoogleMap?.setPadding(0, 40, 0, 0)


        //customizing map controls
        val uiSettings = mGoogleMap!!.uiSettings
        uiSettings.isCompassEnabled = false
        uiSettings.isZoomGesturesEnabled = true
        uiSettings.isRotateGesturesEnabled = false
        uiSettings.isMyLocationButtonEnabled = true


        //for things to do after map is ready
        onMapReadyListener()
    }


    fun moveCamera(latLng: LatLng, markerType: Int) {

        mGoogleMap?.animateCamera(CameraUpdateFactory.newCameraPosition(CameraPosition(latLng, ZOOM_FACTOR, 0f, 0f)))
//        plotSingleMarker(latLng, markerType)

    }

    fun zoomCamera(latLng: LatLng, zoomLevel: Float) {
        mGoogleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel))
    }

    fun plotSingleMarker(latLong: LatLng, markerType: Int) {
        mGoogleMap?.addMarker(MarkerOptions().icon(BitmapDescriptorFactory.fromResource(markerType))
                .position(latLong))
    }

    fun plotMultipleMarkers(latLongList: List<LatLng>, markerType: Int) {
        latLongList.forEach {
            mGoogleMap?.addMarker(MarkerOptions().icon(BitmapDescriptorFactory.fromResource(markerType)).position(it))
        }
    }




//    fun plotMarkerWithIcon(markerList: List<PojoMarker>, icon: Int) {
//
//        //add marker
//        markerList.forEach {
//            mGoogleMap?.addMarker(MarkerOptions().icon(BitmapDescriptorFactory
//                    .fromResource(icon))
//                    .position(it.position)
//                    .snippet(it.mSnippet)
//                    .title(it.mTitle))?.showInfoWindow()
//        }
//    }

    fun plotPolyLines(latLngs: List<LatLng>, markerStart: Int, markerEnd: Int) {

//        //  decoding latLngString
//        val latLongs = PolyUtil.decode(polyLines)

        mGoogleMap?.clear()

        if (latLngs.isEmpty()) return

        val latLongs = mutableListOf<LatLng>()
        latLngs.forEach {
            latLongs.add(LatLng(it.latitude, it.longitude))
        }


        //adjusting bounds
        val latlngBound = LatLngBounds.builder()
        latLongs.forEach {
            latlngBound.include(it)
        }


        //animating camera according to bounds
        mGoogleMap?.animateCamera(CameraUpdateFactory.newLatLngBounds(latlngBound.build(),
                activityContext.resources.getDimension(R.dimen.latlng_bounds_offset).toInt()))


        //adding marker on start and end position in according to latLngList
        plotSingleMarker(latLongs[0], markerStart)
        plotSingleMarker(latLongs[latLongs.size - 1], markerEnd)


        //adding polyLines according to latLngList
        mGoogleMap?.addPolyline(PolylineOptions().addAll(latLongs))

    }


    //draw overlay for easiness
//    fun drawOverLay(latLng: LatLng, radius: Double) {
//        areaOverLay = if (null != areaOverLay) {
//            areaOverLay?.remove()
//            null
//        } else {
//            val circleOptions = CircleOptions()
//                    .center(latLng)
//                    .radius(radius) //radius is in meters
//                    .fillColor(ContextCompat.getColor(activityContext, R.color.colorBlack10))
//                    .strokeColor(ContextCompat.getColor(activityContext, R.color.colorBlack))
//                    .strokeWidth(4f)
//            mGoogleMap?.addCircle(circleOptions)
//        }
//    }

    override fun onMyLocationButtonClick(): Boolean {
        return true
    }

    /**
     * function that will take a string network url and will convert it into bitmap
     */
//    private fun convertUrlToBitMap(urlString: String): Bitmap? {
//        var mBitmap: Bitmap? = null
//
//        val imageRequest = ImageRequest.fromUri(GeneralFunctions.getResizedImage(urlString, 48, 48))
//
//        val dataSource = Fresco.getImagePipeline().fetchDecodedImage(imageRequest, null)
//
//        dataSource.subscribe(object : BaseBitmapDataSubscriber() {
//            override fun onNewResultImpl(bitmap: Bitmap?) {
//                mBitmap = bitmap
//            }
//
//            override fun onFailureImpl(dataSource: DataSource<CloseableReference<CloseableImage>>?) {
//            }
//
//        }, UiThreadImmediateExecutorService.getInstance())
//        return mBitmap
//    }


//    private fun createCustomMarker(imageUrl: String): Bitmap {
//
//
//        val marker = LayoutInflater.from(activityContext).inflate(R.layout.layout_user_marker, null)
//
//        val markerImage = marker.sdvUserImage
//        markerImage.setImageBitmap(convertUrlToBitMap(imageUrl))
//
//        val displayMetrics = DisplayMetrics()
//        (activityContext as BaseAppCompactActivity).windowManager.defaultDisplay.getMetrics(displayMetrics)
//        marker.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
//        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
//        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
//        marker.buildDrawingCache()
//        val bitmap = Bitmap.createBitmap(marker.measuredWidth, marker.measuredHeight, Bitmap.Config.ARGB_8888)
//        val canvas = Canvas(bitmap)
//        marker.draw(canvas)

//        return bitmap
//    }

    abstract fun setMapData()
    abstract fun onMapReadyListener()


}