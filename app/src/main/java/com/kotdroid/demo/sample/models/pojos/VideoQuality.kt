package com.kotdroid.demo.sample.models.pojos

data class VideoQuality(var videoQualityType: String, var videoEncodingRate: Int, var audioEncodingRate: Int)