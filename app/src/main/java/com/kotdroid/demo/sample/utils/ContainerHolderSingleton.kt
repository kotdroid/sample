package com.kotdroid.demo.sample.utils

import com.google.android.gms.tagmanager.ContainerHolder


object ContainerHolderSingleton {

    private var containerHolder: ContainerHolder? = null


    fun getContainerHolder(): ContainerHolder? {
        return containerHolder
    }

    fun setContainerHolder(c: ContainerHolder) {
        containerHolder = c
    }
}