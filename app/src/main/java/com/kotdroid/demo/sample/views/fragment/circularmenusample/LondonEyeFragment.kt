package com.kotdroid.demo.sample.views.fragment.circularmenusample

import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.londoneye.layoutmanager.LondonEyeLayoutManager
import com.kotdroid.demo.sample.londoneye.layoutmanager.scroller.IScrollHandler
import com.kotdroid.demo.sample.models.pojos.Emoji
import com.kotdroid.demo.sample.viewmodels.BaseViewModel
import com.kotdroid.demo.sample.views.adapters.LondonEyeAdapter
import com.kotdroid.demo.sample.views.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_london_eye.*
import java.util.*

class LondonEyeFragment : BaseFragment() {

    private val mLondonEyeAdapter by lazy {
        LondonEyeAdapter(activityContext)
    }

    var list: ArrayList<Emoji>? = null


    override val layoutId: Int
        get() = R.layout.fragment_london_eye

    override val isNavigationBarEnabled: Boolean
        get() = false

    override val viewModel: BaseViewModel?
        get() = null

    override fun init() {


        getList()

        val screenWidth = activityContext.resources.displayMetrics.widthPixels
        val screenHeight = activityContext.resources.displayMetrics.heightPixels

        val circleRadius = screenWidth/2

        val xOrigin = -200
        val yOrigin = screenHeight/2

        recyclerView.setParameters(circleRadius, xOrigin, yOrigin)
        recyclerView.setHasFixedSize(true)

        val mLondonEyeLayoutManager = LondonEyeLayoutManager(
                circleRadius,
                xOrigin,
                yOrigin,
                recyclerView,
                IScrollHandler.Strategy.NATURAL)
        recyclerView.layoutManager = mLondonEyeLayoutManager

        mLondonEyeAdapter.updateList(list!!)
        recyclerView.adapter = mLondonEyeAdapter


    }

    override fun observeData() {}

    private fun getList() {
        list = ArrayList<Emoji>().apply {
            add(Emoji(R.drawable.ic_happy, "Happy"))
            add(Emoji(R.drawable.ic_romentic, "Romantic"))
            add(Emoji(R.drawable.ic_neutral, "Neutral"))
            add(Emoji(R.drawable.ic_sad, "Sad"))
            add(Emoji(R.drawable.ic_calm, "Calm"))
            add(Emoji(R.drawable.ic_funny, "Funny"))
        }
    }
}