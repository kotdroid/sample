package com.kotdroid.demo.sample.views.utils

import android.content.Context
import android.graphics.Rect
import android.util.TypedValue
import android.view.View
import androidx.recyclerview.widget.RecyclerView


internal class RecyclerItemDecoration : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect.top = DpToPx(parent.context, 5f).toInt()
        outRect.bottom = DpToPx(parent.context, 5f).toInt()

        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.top = DpToPx(parent.context, 100f).toInt()
        }
        if (parent.getChildAdapterPosition(view) == parent.adapter!!.itemCount - 1) {
            outRect.bottom = DpToPx(parent.context, 100f).toInt()
        }
    }

    /**
     * Function to convert a value given in dp to pixels (px).
     *
     * @param context Current context, used to access resources.
     * @param dp      The value (in dp) to be converted.
     * @return        The value in pixels.
     */

    private fun DpToPx(context: Context, dp: Float): Float {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics())
    }
}