package com.kotdroid.demo.sample.services

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.utils.GeneralFunctions
import com.kotdroid.demo.sample.views.activity.CameraActivity

class LocationOffService : Service() {

    companion object {

        // Notification channel data
        private const val PACKAGE_NAME = "com.kotdroid.demo.sample"

        private const val GENERAL_NOTIFICATION_CHANNEL_ID = "$PACKAGE_NAME.generalNotification"
        private const val GENERAL_NOTIFICATION_CHANNEL_NAME = "General"


        private const val NOTIFICATION_ID = 123456734

    }

    private var mNotificationManager: NotificationManager? = null

    override fun onCreate() {
        super.onCreate()

        mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    override fun onBind(p0: Intent?): IBinder {
        return null!!
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        //starting foreground service as notification
        // The PendingIntent to launch activity.
        val activityPendingIntent = PendingIntent.getActivity(this, 0,
                Intent(this, CameraActivity::class.java), 0)

        startForeground(NOTIFICATION_ID, getNotification(channelId = GENERAL_NOTIFICATION_CHANNEL_ID
                , channelName = GENERAL_NOTIFICATION_CHANNEL_NAME
                , pendingIntent = activityPendingIntent, contentMessage = "You need to turn on your enable location permission."))

        return START_STICKY
    }


    @SuppressLint("NewApi")
    private fun getNotification(
            contentTitle: String = getString(R.string.app_name),
            contentMessage: String,
            pendingIntent: PendingIntent,
            channelId: String,
            channelName: String
    ): Notification {
        if (isOreoDevice && null == mNotificationManager?.getNotificationChannel(channelId)) {
            val notificationChannel = NotificationChannel(
                    channelId,
                    channelName,
                    NotificationManager.IMPORTANCE_HIGH
            )
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.setShowBadge(true)
            notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            mNotificationManager!!.createNotificationChannel(notificationChannel)
        }
        return NotificationCompat.Builder(
                this, channelId)
                .setContentTitle(contentTitle)
                .setContentText(contentMessage)
                .setStyle(NotificationCompat.BigTextStyle().bigText(contentMessage))
                .setSmallIcon(getNotificationIcon())
                .setTicker(contentTitle)
                .setDefaults(Notification.DEFAULT_ALL)
                .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent)
                .setAutoCancel(true).build()
    }

    private val isOreoDevice: Boolean
        get() = android.os.Build.VERSION_CODES.O <= android.os.Build.VERSION.SDK_INT

    private fun getNotificationIcon(): Int {
        return if (GeneralFunctions.isAboveLollipopDevice)
            R.mipmap.ic_launcher
        else
            R.mipmap.ic_launcher
    }
}