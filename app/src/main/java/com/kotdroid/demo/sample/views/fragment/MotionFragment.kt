package com.kotdroid.demo.sample.views.fragment

import androidx.recyclerview.widget.RecyclerView
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.viewmodels.BaseViewModel
import com.kotdroid.demo.sample.views.adapters.MotionAdapter

class MotionFragment : BaseRecyclerViewFragment() {


    private val mMotionAdapter by lazy {
        MotionAdapter(this)
    }

    override val layoutId: Int
        get() = R.layout.motion_layout

    override val isNavigationBarEnabled: Boolean
        get() = false

    override val viewModel: BaseViewModel?
        get() = null

    override val recyclerViewAdapter: RecyclerView.Adapter<*>?
        get() = mMotionAdapter

    override val layoutManager: RecyclerView.LayoutManager?
        get() = null

    override val isShowRecyclerViewDivider: Boolean
        get() = true

    override fun setData() {

    }

    override fun onPullDownToRefresh() {

    }

    override fun observeData() {

    }
}