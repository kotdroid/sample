package com.kotdroid.demo.sample.viewmodels

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng

abstract class BaseLocationViewModel(application: Application) : BaseViewModel(application) {

    private var userLocation = MutableLiveData<LatLng>()

    fun setUserLocation(latLng: LatLng) {
        userLocation.postValue(latLng)
    }

    fun getUserLocation(): LiveData<LatLng> = userLocation
}