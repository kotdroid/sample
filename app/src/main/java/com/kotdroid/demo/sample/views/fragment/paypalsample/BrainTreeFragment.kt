package com.kotdroid.demo.sample.views.fragment.paypalsample

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.braintreepayments.api.BraintreeFragment
import com.braintreepayments.api.dropin.DropInActivity
import com.braintreepayments.api.dropin.DropInRequest
import com.braintreepayments.api.dropin.DropInResult
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.viewmodels.BaseViewModel
import com.kotdroid.demo.sample.views.activity.HomeActivity
import com.kotdroid.demo.sample.views.fragment.BaseFragment
import com.paypal.android.sdk.payments.PayPalConfiguration
import com.paypal.android.sdk.payments.PayPalPayment
import com.paypal.android.sdk.payments.PayPalService
import com.paypal.android.sdk.payments.PaymentActivity
import kotlinx.android.synthetic.main.fragment_braine_tree.*
import java.math.BigDecimal


class BrainTreeFragment : BaseFragment() {
    companion object {
        const val PAYPAL_REQUEST_CODE = 78
    }

    private var mPayPalConfig: PayPalConfiguration? = null

    override val layoutId: Int
        get() = R.layout.fragment_braine_tree

    override val isNavigationBarEnabled: Boolean
        get() = false

    override val viewModel: BaseViewModel?
        get() = null

    override fun init() {
//        val dropInRequest = DropInRequest()
//                .clientToken(clientToken)
//        startActivityForResult(dropInRequest.getIntent(activityContext), 67)


        // Initialize PayPalConfig
        mPayPalConfig = PayPalConfiguration().apply {
            environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            clientId(PayPalConfig.PAYPAL_CLIENT_ID)
        }
        btnPayPal.setOnClickListener {
//            val mBraintreeFragment = BraintreeFragment.newInstance(activity, PayPalConfig.PAYPAL_CLIENT_ID)
//
//            (activityContext as HomeActivity).fragmentManager.beginTransaction().add(R.id.flContainerHome,
//                    mBraintreeFragment).commit()
//
//
//            val dropInRequest = DropInRequest().clientToken(PayPalConfig.PAYPAL_CLIENT_ID)
//
//
//            (activityContext as HomeActivity).startActivityForResult(dropInRequest
//                    .getIntent(activityContext), PAYPAL_REQUEST_CODE)

                        getPayment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        val payPalServiceIntent = Intent(activityContext, PayPalService::class.java)
//        payPalServiceIntent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, mPayPalConfig)
//        activityContext.startService(payPalServiceIntent)
    }

    override fun observeData() {}

    private fun getPayment() {

        if (etAmount.text.toString().isEmpty()) {
            showMessage(message = getString(R.string.payment_amount))
            return
        }

        // Creating a PayPalPayment
        val paypalPayment = PayPalPayment(BigDecimal(etAmount.text.toString().trim()), "USD",
                "Sample Application Testing", PayPalPayment.PAYMENT_INTENT_SALE)

        // Creating PayPalActivity Intent
        val paymentActivityIntent = Intent(activityContext, PaymentActivity::class.java)
        // Putting paypalPayment as Extra
        paymentActivityIntent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, mPayPalConfig)
        paymentActivityIntent.putExtra(PaymentActivity.EXTRA_PAYMENT, paypalPayment)

        // Start Payment
        (activityContext as HomeActivity).startActivityForResult(paymentActivityIntent, PAYPAL_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (requestCode == PAYPAL_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val result = data.getParcelableExtra<DropInResult>(DropInResult.EXTRA_DROP_IN_RESULT)
                    result.paymentMethodNonce
                }
                // use the result to update your UI and send the payment method nonce to your server
                Activity.RESULT_CANCELED -> {
                    // the user canceled
                }
                else -> {
                    // handle errors here, an exception may be available in
                    val error = data.getSerializableExtra(DropInActivity.EXTRA_ERROR) as Exception
                }
            }
        }
    }

    override fun onDestroy() {
        activityContext.stopService(Intent(activityContext, PayPalService::class.java))
        super.onDestroy()
    }

}