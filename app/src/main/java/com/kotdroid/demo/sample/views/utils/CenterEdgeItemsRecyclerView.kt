package com.kotdroid.demo.sample.views.utils

import android.annotation.TargetApi
import android.content.Context
import android.graphics.PointF
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewTreeObserver.OnPreDrawListener
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView

@TargetApi(23)
class CenterEdgeItemsRecyclerView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) : RecyclerView(context, attrs, defStyle) {
    private val mPaddingPreDrawListener: OnPreDrawListener
    private var mCenterEdgeItems: Boolean = false
    private var mCenterEdgeItemsWhenThereAreChildren: Boolean = false
    private var mOriginalPaddingTop: Int = 0
    private var mOriginalPaddingBottom: Int = 0

    init {
        this.mOriginalPaddingTop = NO_VALUE
        this.mOriginalPaddingBottom = NO_VALUE
        this.mPaddingPreDrawListener = OnPreDrawListener {
            if (this@CenterEdgeItemsRecyclerView.mCenterEdgeItemsWhenThereAreChildren && this@CenterEdgeItemsRecyclerView.childCount > 0) {
                this@CenterEdgeItemsRecyclerView.setupCenteredPadding()
                this@CenterEdgeItemsRecyclerView.mCenterEdgeItemsWhenThereAreChildren = false
            }

            true
        }
        this.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
    }

    override fun onGenericMotionEvent(ev: MotionEvent): Boolean {
        val layoutManager = this.layoutManager
        return layoutManager != null && !this.isLayoutFrozen && super.onGenericMotionEvent(ev)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        this.viewTreeObserver.addOnPreDrawListener(this.mPaddingPreDrawListener)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        this.viewTreeObserver.removeOnPreDrawListener(this.mPaddingPreDrawListener)
    }

    fun setCenterEdgeItems(centerEdgeItems: Boolean) {
        this.mCenterEdgeItems = centerEdgeItems
        if (this.mCenterEdgeItems) {
            if (this.childCount > 0) {
                this.setupCenteredPadding()
            } else {
                this.mCenterEdgeItemsWhenThereAreChildren = true
            }
        } else {
            this.setupOriginalPadding()
            this.mCenterEdgeItemsWhenThereAreChildren = false
        }
    }

    private fun setupCenteredPadding() {
        if (this.mCenterEdgeItems && this.childCount >= 1) {
            val child = this.getChildAt(0)
            val height = child.height
            val desiredPadding = (this.height.toFloat() * 0.5f - height.toFloat() * 0.5f).toInt()
            if (this.paddingTop != desiredPadding) {
                this.mOriginalPaddingTop = this.paddingTop
                this.mOriginalPaddingBottom = this.paddingBottom
                this.setPadding(this.paddingLeft, desiredPadding, this.paddingRight, desiredPadding)
                val focusedChild = this.focusedChild
                val focusedPosition = if (focusedChild != null) this.layoutManager!!.getPosition(focusedChild) else 0
                this.layoutManager!!.scrollToPosition(focusedPosition)
            }

        } else {
            Log.w(TAG, "No children available")
        }
    }

    private fun setupOriginalPadding() {
        if (this.mOriginalPaddingTop != NO_VALUE) {
            this.setPadding(this.paddingLeft, this.mOriginalPaddingTop, this.paddingRight, this.mOriginalPaddingBottom)
        }
    }

    abstract class ChildLayoutManager : LinearLayoutManager {
        private val factor: Float

        constructor(context: Context) : super(context, LinearLayoutManager.HORIZONTAL, false) {
            factor = 1.0f
        }

        internal constructor(context: Context, factor: Float) : super(context, LinearLayoutManager.VERTICAL, false) {
            this.factor = factor
        }

        override fun scrollVerticallyBy(dy: Int, recycler: RecyclerView.Recycler?, state: RecyclerView.State?): Int {
            val scrolled = super.scrollVerticallyBy(dy, recycler, state)
            this.updateLayout()
            return scrolled
        }

        override fun onLayoutChildren(recycler: RecyclerView.Recycler?, state: RecyclerView.State) {
            super.onLayoutChildren(recycler, state)
            if (this.childCount != 0) {
                this.updateLayout()
            }
        }

        override fun smoothScrollToPosition(recyclerView: RecyclerView, state: RecyclerView.State?, position: Int) {

            val linearSmoothScroller = object : LinearSmoothScroller(recyclerView.context) {

                override fun computeScrollVectorForPosition(targetPosition: Int): PointF? {
                    return this@ChildLayoutManager.computeScrollVectorForPosition(targetPosition)
                }

                override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics): Float {
                    return super.calculateSpeedPerPixel(displayMetrics) * factor
                }
            }

            linearSmoothScroller.targetPosition = position
            startSmoothScroll(linearSmoothScroller)
        }

        private fun updateLayout() {
            for (count in 0 until this.childCount) {
                val child = this.getChildAt(count)
                this.updateChild(child, child!!.parent as CenterEdgeItemsRecyclerView)
            }

        }

        override fun canScrollHorizontally(): Boolean {
            return false
        }

        abstract fun updateChild(var1: View?, var2: CenterEdgeItemsRecyclerView)
    }

    companion object {
        private val TAG = CenterEdgeItemsRecyclerView::class.java.simpleName
        private const val NO_VALUE = -2147483648
    }
}
