package com.kotdroid.demo.sample.views.fragment.mobilvisionsample

import android.app.AlertDialog
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import com.google.android.gms.vision.Frame
import com.google.android.gms.vision.face.FaceDetector
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.viewmodels.BaseViewModel
import com.kotdroid.demo.sample.views.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_mobile_vision.*


class MobileVisionFragment : BaseFragment() {

    override val layoutId: Int
        get() = R.layout.fragment_mobile_vision

    override val isNavigationBarEnabled: Boolean
        get() = false

    override val viewModel: BaseViewModel?
        get() = null

    override fun init() {

        val myBitmap = getBitmap()

        // Creating Canvass Object
        val tempBitmap = Bitmap.createBitmap(myBitmap.width, myBitmap.height, Bitmap.Config.RGB_565)
        val tempCanvas = Canvas(tempBitmap)
        tempCanvas.drawBitmap(myBitmap, 0f, 0f, null)


        // Creating FaceDetector
        val faceDetector = FaceDetector.Builder(activityContext).setTrackingEnabled(false)
                .build()
        if (!faceDetector.isOperational) {
            AlertDialog.Builder(activityContext).setMessage("Could not set up the face detector!").show()
            return
        }

        // Detecting Face
        val frame = Frame.Builder().setBitmap(myBitmap).build()
        val faces = faceDetector.detect(frame)





        btnProcess.setOnClickListener {
            ivImage.setImageResource(R.drawable.suraj_panda)
            //Draw Rectangle on Face
            for (i in 0 until faces.size()) {
                val thisFace = faces.valueAt(i)
                val x1 = thisFace.position.x
                val y1 = thisFace.position.y
                val x2 = x1 + thisFace.width
                val y2 = y1 + thisFace.height
                tempCanvas.drawRoundRect(RectF(x1, y1, x2, y2), 2f, 2f, getPaint())
            }

            ivImageProcessed.setImageDrawable(BitmapDrawable(resources, tempBitmap))
        }
    }

    override fun observeData() {}

    private fun getBitmap(resource: Int = 0): Bitmap {
        val bitMapFactoryOption = BitmapFactory.Options()
        bitMapFactoryOption.inMutable = true
        return BitmapFactory.decodeResource(resources, R.drawable.suraj_panda, bitMapFactoryOption)
    }

    private fun getPaint(): Paint {
        val myRectPaint = Paint()
        myRectPaint.strokeWidth = 5f
        myRectPaint.color = Color.RED
        myRectPaint.style = Paint.Style.STROKE

        return myRectPaint
    }
}