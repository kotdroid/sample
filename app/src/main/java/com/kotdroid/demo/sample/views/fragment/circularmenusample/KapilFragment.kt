package com.kotdroid.demo.sample.views.fragment.circularmenusample

import com.kapil.circularlayoutmanager.CircularLayoutManager
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.viewmodels.BaseViewModel
import com.kotdroid.demo.sample.views.adapters.RecyclerAdapter
import com.kotdroid.demo.sample.views.fragment.BaseFragment
import com.kotdroid.demo.sample.views.utils.RecyclerItemDecoration
import kotlinx.android.synthetic.main.fragment_kapil.*


class KapilFragment : BaseFragment() {

    override val layoutId: Int
        get() = R.layout.fragment_kapil

    override val isNavigationBarEnabled: Boolean
        get() = false

    override val viewModel: BaseViewModel?
        get() = null


    override fun init() {
        scrollWheel.recyclerView = recyclerView
        scrollWheel.isScrollWheelEnabled = false
        scrollWheel.isHighlightTouchAreaEnabled = true
        scrollWheel.touchAreaThickness = 50

//        recyclerView.addItemDecoration(RecyclerItemDecoration())

        recyclerView.layoutManager = CircularLayoutManager(activityContext, 200, -100)
        recyclerView.addItemDecoration(RecyclerItemDecoration())


        recyclerView.adapter = RecyclerAdapter()


    }

    override fun observeData() {

    }
}