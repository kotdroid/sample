package com.kotdroid.demo.sample.views.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.views.activity.inflate

class RecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyHolder(parent.inflate(layoutRes = R.layout.row_list))
    }

    override fun getItemCount(): Int {
        return 30
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    }

    internal class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}
}