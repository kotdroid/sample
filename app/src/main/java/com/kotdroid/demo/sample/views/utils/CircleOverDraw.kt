package com.kotdroid.demo.sample.views.utils

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import com.kotdroid.demo.sample.R

class CircleOverDraw constructor(context: Context, attributeSet: AttributeSet) : View(context, attributeSet) {

    var mRadius = 0f
    var xCenter = 0f
    var yCenter = 0f


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        val paint = Paint()
        paint.isAntiAlias = false
        paint.style = Paint.Style.FILL_AND_STROKE
        paint.color = ContextCompat.getColor(context, R.color.colorBlack10)
        paint.strokeWidth = 5f

        //drawCircle
        canvas?.drawCircle(xCenter, yCenter, mRadius, paint)
        invalidate()
    }
}