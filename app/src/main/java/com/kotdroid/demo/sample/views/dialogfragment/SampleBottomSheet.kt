package com.kotdroid.demo.sample.views.dialogfragment

import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.models.pojos.Emoji
import com.kotdroid.demo.sample.views.adapters.BottomSheetAdapter
import kotlinx.android.synthetic.main.dialog_fragment_sample_bottom_sheet.*
import java.util.*

class SampleBottomSheet : BaseBottomSheetDialogFragment() {

    private val bottomSheetAdapter by lazy { BottomSheetAdapter() }
    override val layoutId: Int
        get() = R.layout.dialog_fragment_sample_bottom_sheet

    override fun init() {

        recyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerView.adapter = bottomSheetAdapter
        recyclerView.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))

        val list = ArrayList<Emoji>().apply {
            add(Emoji(R.drawable.ic_happy, "Happy"))
            add(Emoji(R.drawable.ic_romentic, "Romantic"))
            add(Emoji(R.drawable.ic_neutral, "Neutral"))
            add(Emoji(R.drawable.ic_sad, "Sad"))
            add(Emoji(R.drawable.ic_calm, "Calm"))
            add(Emoji(R.drawable.ic_funny, "Funny"))
            add(Emoji(R.drawable.ic_happy, "Happy"))
            add(Emoji(R.drawable.ic_romentic, "Romantic"))
            add(Emoji(R.drawable.ic_neutral, "Neutral"))
            add(Emoji(R.drawable.ic_sad, "Sad"))
            add(Emoji(R.drawable.ic_calm, "Calm"))
            add(Emoji(R.drawable.ic_funny, "Funny"))
            add(Emoji(R.drawable.ic_happy, "Happy"))
            add(Emoji(R.drawable.ic_romentic, "Romantic"))
            add(Emoji(R.drawable.ic_neutral, "Neutral"))
            add(Emoji(R.drawable.ic_sad, "Sad"))
            add(Emoji(R.drawable.ic_calm, "Calm"))
            add(Emoji(R.drawable.ic_funny, "Funny"))
            add(Emoji(R.drawable.ic_happy, "Happy"))
            add(Emoji(R.drawable.ic_romentic, "Romantic"))
            add(Emoji(R.drawable.ic_neutral, "Neutral"))
            add(Emoji(R.drawable.ic_sad, "Sad"))
            add(Emoji(R.drawable.ic_calm, "Calm"))
            add(Emoji(R.drawable.ic_funny, "Funny"))
        }

        bottomSheetAdapter.updateList(list)
    }
}