package com.kotdroid.demo.sample.views.fragment.camerasample

import android.view.View
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.viewmodels.BaseViewModel
import com.kotdroid.demo.sample.views.activity.CameraActivity
import com.kotdroid.demo.sample.views.activity.HomeActivity
import kotlinx.android.synthetic.main.fragment_preview.*

class CameraFragment : BaseCameraFragment(), View.OnClickListener {


    override val layoutId: Int
        get() = R.layout.fragment_preview

    override val isNavigationBarEnabled: Boolean
        get() = false

    override val viewModel: BaseViewModel?
        get() = null

    override fun setData() {

        // Set CLick Listener
        ivCapture.setOnClickListener(this)
        ivVideoIcon.setOnClickListener(this)
        ivToggleCamera.setOnClickListener(this)
    }

    override fun observeData() {
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.ivCapture -> {
                lockFocus()
            }
            R.id.ivVideoIcon -> {
                super.onPause()
                (activityContext as HomeActivity).openRequiredFragment(HomeActivity.FRAGMENT_TYPE_VIDEO)
            }
            R.id.ivToggleCamera -> {
                toggleCamera()
            }
        }
    }

}