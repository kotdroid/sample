package com.kotdroid.demo.sample.services

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.IBinder
import android.widget.Toast
import com.google.android.gms.location.*
import com.kotdroid.demo.sample.utils.NotificationUtils

class LocationUpdateService : Service() {

    companion object {
        private const val PACKAGE_NAME = "com.app.trackingguru"
        const val EXTRA_STARTED_FROM_NOTIFICATION = "$PACKAGE_NAME.startedFromNotification"
        private const val NOTIFICATION_ID = 12345678
    }


    //for getting current location
    private var mLocation: Location? = null
    private lateinit var mLocationCallback: LocationCallback
    private lateinit var mFusedLocationProviderClient: FusedLocationProviderClient

    //for creating foreground services notification
    private val mNotificationUtils by lazy { NotificationUtils(this) }

    override fun onBind(p0: Intent?): IBinder {
        return null!!
    }


    @SuppressLint("MissingPermission")
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {


        //initializing mFusedLocationProviderClient
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        //initializing location callback
        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                onNewLocation(locationResult!!)
            }
        }

        //creating customized locationRequestObject
        val mLocationRequest = LocationRequest.create().apply {
            interval = 5 * 10000
            fastestInterval = 20000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        //requesting for location update from fusedLocationProvider
        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback, null)


        //starting foreground service as notification
        startForeground(NOTIFICATION_ID,
                mNotificationUtils.getLocationUpdateNotification(mLocation).build())

        return START_STICKY
    }


    /**
     * this function is called every time when fusedLocationProvider provides new location.
     */
    fun onNewLocation(locationResult: LocationResult) {
        // Update notification content if running as a foreground foregroundService.
        if (serviceIsRunningInForeground(this)) {
            mNotificationUtils.notificationManager.notify(NOTIFICATION_ID,
                    mNotificationUtils.getLocationUpdateNotification(mLocation).build())
            mLocation = locationResult.lastLocation

            if (serviceIsRunningInForeground(this)) {
                mNotificationUtils.notificationManager.notify(NOTIFICATION_ID,
                        mNotificationUtils.getLocationUpdateNotification(mLocation).build())
            }

            Toast.makeText(this, "Service - lat : ${mLocation?.latitude} " +
                    "lng :${mLocation?.longitude}", Toast.LENGTH_SHORT).show()

        }
    }


    //method to check if the service is running in foreground
    @Suppress("DEPRECATION")
    private fun serviceIsRunningInForeground(context: Context): Boolean {
        val manager = context.getSystemService(
                Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(
                Integer.MAX_VALUE)) {
            if (javaClass.name == service.service.className) {
                if (service.foreground) {
                    return true
                }
            }
        }
        return false
    }

}

