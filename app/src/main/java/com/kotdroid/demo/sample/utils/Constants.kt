package com.kotdroid.demo.sample.utils

import android.os.Environment
import com.google.android.gms.maps.model.LatLng


object Constants {

    private const val APP_NAME = "Sample"

     var LOCATION_INTERVAL: Long = 1000
     const val  FASTEST_LOCATION_INTERVAL: Long = 10000

    // Media Constants
    const val LOCAL_FILE_PREFIX = "file://"
    private val LOCAL_STORAGE_BASE_PATH_FOR_MEDIA = Environment
            .getExternalStorageDirectory().toString() + "/" + APP_NAME
    val LOCAL_STORAGE_BASE_PATH_FOR_USER_PHOTOS = "$LOCAL_STORAGE_BASE_PATH_FOR_MEDIA/User/Photos/"
    val LOCAL_STORAGE_BASE_PATH_FOR_USER_VIDEOS = "$LOCAL_STORAGE_BASE_PATH_FOR_MEDIA/User/Videos/"
}