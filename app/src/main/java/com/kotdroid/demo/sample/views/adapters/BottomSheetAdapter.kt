package com.kotdroid.demo.sample.views.adapters

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.models.pojos.Emoji
import com.kotdroid.demo.sample.views.activity.inflate
import kotlinx.android.synthetic.main.row_sample_bottom_sheet.view.*

class BottomSheetAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var emojiList = ArrayList<Emoji>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return Holder(parent.inflate(layoutRes = R.layout.row_sample_bottom_sheet))
    }

    @Suppress("NAME_SHADOWING")
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as Holder).bindHolder(position)
    }

    override fun getItemCount(): Int {
        return emojiList.size
    }

    fun updateList(list: ArrayList<Emoji>) {
        emojiList.clear()
        emojiList.addAll(list)
        notifyDataSetChanged()
    }

    private inner class Holder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                //                Toast.makeText(context, "Item no.${adapterPosition + 1} Clicked.", Toast.LENGTH_SHORT).show()
                //manager.smoothScrollToPosition(adapterPosition);
            }
        }

        @Suppress("NAME_SHADOWING")
        @SuppressLint("SetTextI18n")
        fun bindHolder(position: Int) {
            itemView.tvEmojiName.text = emojiList[position].name
            itemView.ivImage.setImageResource(emojiList[position].icon)

        }
    }
}
