package com.kotdroid.demo.sample.models.pojos

import androidx.annotation.StringRes


data class RetrofitErrorMessage(@StringRes val errorResId: Int? = null, val errorMessage: String? = null)