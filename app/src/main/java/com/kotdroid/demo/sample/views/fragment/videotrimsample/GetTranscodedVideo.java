package com.kotdroid.demo.sample.views.fragment.videotrimsample;

/**
 * Created by cbll88 on 28/11/17.
 */

public interface GetTranscodedVideo {
    void getTranscoded(String fileVideo, Boolean isPass);
}
