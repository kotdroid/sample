package com.kotdroid.demo.sample.utils

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.services.LocationUpdateService
import com.kotdroid.demo.sample.views.activity.CameraActivity
import java.util.*

/**
 * Created by user007 on 13/6/18.
 */

class NotificationUtils constructor(base: Context) : ContextWrapper(base) {


    companion object {
        const val LOCATION_CHANNEL_ID = "locationNotification"
        const val LOCATION_CHANNEL_NAME = "Location Notification"

        const val TEST_CHANNEL_ID = "testNotification"
        const val TEST_CHANNEL_NAME = "Test Notification"

        const val RIDE_CHANNEL_ID = "rideNotification"
        const val RIDE_CHANNEL_NAME = "Ride Notification"

    }


    val notificationManager: NotificationManager get() = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannel() {
        //creating channel objects with unique circle_id

        val locationNotificationChannel = NotificationChannel(LOCATION_CHANNEL_ID,
                LOCATION_CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW)
        locationNotificationChannel.lightColor = Color.CYAN
        locationNotificationChannel.setShowBadge(true)
        locationNotificationChannel.lightColor = Color.GREEN
        locationNotificationChannel.setShowBadge(true)
        locationNotificationChannel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        locationNotificationChannel.lightColor = Color.GREEN
        notificationManager.createNotificationChannel(locationNotificationChannel)

        val testNotifi = NotificationChannel(TEST_CHANNEL_ID, TEST_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT)
        notificationManager.createNotificationChannel(testNotifi)

        val rideNotificationChannel = NotificationChannel(RIDE_CHANNEL_ID, RIDE_CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW)
        rideNotificationChannel.lightColor = Color.CYAN
        rideNotificationChannel.setShowBadge(true)
        rideNotificationChannel.lightColor = Color.GREEN
        rideNotificationChannel.setShowBadge(true)
        rideNotificationChannel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        rideNotificationChannel.lightColor = Color.GREEN
        notificationManager.createNotificationChannel(rideNotificationChannel)
    }


    fun getLocationUpdateNotification(location: Location?): NotificationCompat.Builder {

        val intent = Intent(this, LocationUpdateService::class.java)


        // Extra to help us figure out if we arrived in onStartCommand via the notification or not.
        intent.putExtra(LocationUpdateService.EXTRA_STARTED_FROM_NOTIFICATION, true)

        // The PendingIntent that leads to a call to onStartCommand() in this foregroundService.
        val servicePendingIntent = PendingIntent.getService(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT)


        // The PendingIntent to launch activity.
        val activityPendingIntent = PendingIntent.getActivity(this, 0,
                Intent(this, CameraActivity::class.java), 0)

        val title = if (null == location) "Please enable location" else "Location Updated On :" + Date(System.currentTimeMillis())
        val contentText = if (null == location) " " else "lat : ${location.latitude} lng :${location.longitude}"

        return NotificationCompat.Builder(this, LOCATION_CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(contentText)
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .addAction(R.drawable.ic_launcher_background, getString(R.string.launch_activity),
                        activityPendingIntent)
                .setTicker("ABCDESF")
                .setChannelId(LOCATION_CHANNEL_ID)
                .setWhen(System.currentTimeMillis())
    }


    fun getVideoSamplingNotification(percentage: Int?, time: String): NotificationCompat.Builder {

        val intent = Intent(this, LocationUpdateService::class.java)


        // Extra to help us figure out if we arrived in onStartCommand via the notification or not.
        intent.putExtra(LocationUpdateService.EXTRA_STARTED_FROM_NOTIFICATION, true)

        // The PendingIntent that leads to a call to onStartCommand() in this foregroundService.
        val servicePendingIntent = PendingIntent.getService(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT)


        // The PendingIntent to launch activity.
        val activityPendingIntent = PendingIntent.getActivity(this, 0,
                Intent(this, CameraActivity::class.java), 0)


        if (percentage == 1) {
            val calender = Calendar.getInstance()
            val title = "Started at ${calender.get(Calendar.HOUR_OF_DAY)}:${calender.get(Calendar.MINUTE)}:${calender.get(Calendar.SECOND)}:${calender.get(Calendar.MILLISECOND)}"
            ApplicationGlobal.time = title
        }

        return NotificationCompat.Builder(this, LOCATION_CHANNEL_ID)
                .setContentTitle("Video Sampling ==> ${ApplicationGlobal.time}")
                .setContentText("Completed : $time")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .addAction(R.drawable.ic_launcher_background, getString(R.string.launch_activity),
                        activityPendingIntent)
                .setTicker("ABCDESF")
                .setChannelId(LOCATION_CHANNEL_ID)
                .setWhen(System.currentTimeMillis())
    }


}
