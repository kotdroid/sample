package com.kotdroid.demo.sample.utils

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.util.Log
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

object GeneralFunctions {

    private const val ALPHA_NUMERIC_CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
    private const val JPEG_FILE_PREFIX = "IMG_"
    private const val JPEG_FILE_SUFFIX = ".jpg"
    private const val MP4_FILE_SUFFIX = ".mp4"
    private const val MP4_FILE_PREFIX = "VIDEO_"
    private const val MIN_PASSWORD_LENGTH = 6
    private const val MAX_PASSWORD_LENGTH = 15

    private var isInChatScreen: Boolean = false
    private var inChatConversationId: Int = -1

    private var activeCircleId = -1

    val isAboveLollipopDevice: Boolean
        get() = Build.VERSION_CODES.LOLLIPOP <= Build.VERSION.SDK_INT

    fun generateRandomString(randomStringLength: Int): String {
        val buffer = StringBuffer()
        val charactersLength = ALPHA_NUMERIC_CHARS.length
        for (i in 0 until randomStringLength) {
            val index = Math.random() * charactersLength
            buffer.append(ALPHA_NUMERIC_CHARS[index.toInt()])
        }
        return buffer.toString()
    }

    @Throws(IOException::class)
    fun setUpImageFile(directory: String): File? {
        var imageFile: File? = null
        if (Environment.MEDIA_MOUNTED == Environment
                        .getExternalStorageState()) {
            val storageDir = File(directory)
            if (!storageDir.mkdirs()) {
                if (!storageDir.exists()) {
                    Log.d("CameraSample", "failed to create directory")
                    return null
                }
            }

            imageFile = File.createTempFile(JPEG_FILE_PREFIX
                    + System.currentTimeMillis() + "_",
                    JPEG_FILE_SUFFIX, storageDir)
        }
        return imageFile
    }

    @Throws(IOException::class)
    fun setUpVideoFile(directory: String): File? {
        var videoFile: File? = null
        if (Environment.MEDIA_MOUNTED == Environment
                        .getExternalStorageState()) {
            val storageDir = File(directory)
            if (!storageDir.mkdirs()) {
                if (!storageDir.exists()) {
                    Log.d("CameraSample", "failed to create directory")
                    return null
                }
            }

            videoFile = File.createTempFile(MP4_FILE_PREFIX
                    + System.currentTimeMillis() + "_",
                    MP4_FILE_SUFFIX, storageDir)
        }
        return videoFile
    }

    fun createVideoDirectory(directory: String): String {
        if (Environment.MEDIA_MOUNTED == Environment
                        .getExternalStorageState()) {
            val storageDir = File(directory)
            if (!storageDir.mkdirs()) {
                if (!storageDir.exists()) {
                    Log.d("CameraSample", "failed to create directory")
                }
            }
        }

        return directory
    }

    fun isValidEmail(target: CharSequence?): Boolean {
        return null != target && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }

    fun isValidPassword(password: String): Boolean {
        return password.length in MIN_PASSWORD_LENGTH..MAX_PASSWORD_LENGTH
    }

    val timeZone: String
        get() = TimeZone.getDefault().id

    fun getResizedImage(imageUrl: String, imageWidth: Int = 0, imageHeight: Int = 0): String {
        return imageUrl + (if (0 != imageWidth) "/$imageWidth" else "") +
                if (0 != imageHeight) "/$imageHeight" else ""
    }

    fun getLocalImageUri(file: File): Uri {
        return Uri.parse("file://$file")
    }

    @SuppressLint("SimpleDateFormat")
    fun getVideoFileName(): String {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        return "VIDEO$timeStamp.mp4"
    }


    fun getImageUrlFromLatLng(latlng: String, width: Int, height: Int): String {
        return ("http://maps.googleapis.com/maps/api/staticmap?zoom=17&size="
                + width
                + "x"
                + height
                + "&maptype=roadmap&markers="
                + latlng + "&sensor=false")
    }
}