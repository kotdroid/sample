package com.kotdroid.demo.sample.views.fragment

import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.models.pojos.Emoji
import com.kotdroid.demo.sample.viewmodels.BaseViewModel
import com.kotdroid.demo.sample.views.adapters.CircularEmojiAdapter
import com.kotdroid.demo.sample.views.dialogfragment.SampleBottomSheet
import kotlinx.android.synthetic.main.fragment_bottom_sheet.*
import java.util.*

class BottomSheetFragment : BaseFragment() {


    var behavior: BottomSheetBehavior<*>? = null

    var sampleBottomSheet: SampleBottomSheet? = null

    override val layoutId: Int
        get() = R.layout.fragment_bottom_sheet

    override val isNavigationBarEnabled: Boolean
        get() = false
    override val viewModel: BaseViewModel?
        get() = null

    override fun init() {


        btnBottomSheet.setOnClickListener {
            //showBottomSheet()
            sampleBottomSheet = SampleBottomSheet()
            sampleBottomSheet?.show(fragmentManager, "SampleBottomSheet")
        }


        btnBottomSheetHide.setOnClickListener {
            //            if (BottomSheetBehavior.STATE_EXPANDED == behavior?.state) {
//                behavior?.state = BottomSheetBehavior.STATE_COLLAPSED
//
//            }

            sampleBottomSheet?.dismiss()
        }
    }

    override fun observeData() {}

    private fun showBottomSheet() {
        val bottomSheets = view?.findViewById<ConstraintLayout>(R.id.clBottomSheet)

        val recyclerView = bottomSheets?.findViewById<RecyclerView>(R.id.recyclerViewBottomSheet)

        val circleEmojiAdapter = CircularEmojiAdapter(activityContext)

        recyclerView?.layoutManager = LinearLayoutManager(activityContext, RecyclerView.VERTICAL, false)
        recyclerView?.adapter = circleEmojiAdapter


        val list = ArrayList<Emoji>().apply {
            add(Emoji(R.drawable.ic_happy, "Happy"))
            add(Emoji(R.drawable.ic_romentic, "Romantic"))
            add(Emoji(R.drawable.ic_neutral, "Neutral"))
            add(Emoji(R.drawable.ic_sad, "Sad"))
            add(Emoji(R.drawable.ic_calm, "Calm"))
            add(Emoji(R.drawable.ic_funny, "Funny"))
            add(Emoji(R.drawable.ic_happy, "Happy"))
            add(Emoji(R.drawable.ic_romentic, "Romantic"))
            add(Emoji(R.drawable.ic_neutral, "Neutral"))
            add(Emoji(R.drawable.ic_sad, "Sad"))
            add(Emoji(R.drawable.ic_calm, "Calm"))
            add(Emoji(R.drawable.ic_funny, "Funny"))
            add(Emoji(R.drawable.ic_happy, "Happy"))
            add(Emoji(R.drawable.ic_romentic, "Romantic"))
            add(Emoji(R.drawable.ic_neutral, "Neutral"))
            add(Emoji(R.drawable.ic_sad, "Sad"))
            add(Emoji(R.drawable.ic_calm, "Calm"))
            add(Emoji(R.drawable.ic_funny, "Funny"))
            add(Emoji(R.drawable.ic_happy, "Happy"))
            add(Emoji(R.drawable.ic_romentic, "Romantic"))
            add(Emoji(R.drawable.ic_neutral, "Neutral"))
            add(Emoji(R.drawable.ic_sad, "Sad"))
            add(Emoji(R.drawable.ic_calm, "Calm"))
            add(Emoji(R.drawable.ic_funny, "Funny"))
        }

        circleEmojiAdapter.updateList(list)

        behavior = BottomSheetBehavior.from(bottomSheets)
        behavior!!.state = BottomSheetBehavior.STATE_EXPANDED

    }
}