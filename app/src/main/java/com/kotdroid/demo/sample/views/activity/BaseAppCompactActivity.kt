package com.kotdroid.demo.sample.views.activity

import android.os.Bundle
import androidx.annotation.AnimatorRes
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.utils.GeneralFunctions

abstract class BaseAppCompactActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        if (GeneralFunctions.isAboveLollipopDevice) {
            val window = window
            if (isMakeStatusBarTransparent) {
                window.statusBarColor = ContextCompat.getColor(this, R.color.colorTransparent)
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            } else {
                window.statusBarColor = ContextCompat.getColor(this,
                        R.color.colorPrimaryDark)
            }
        }

        init()
    }

    override fun onBackPressed() {
        if (null != supportFragmentManager && 1 < supportFragmentManager.backStackEntryCount) {
            supportFragmentManager.popBackStackImmediate()
        } else {
            finish()
            super.onBackPressed()
        }
    }


    abstract val layoutId: Int

    abstract val isMakeStatusBarTransparent: Boolean

    abstract fun init()

}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

fun AppCompatActivity.doFragmentTransaction(fragManager: FragmentManager = supportFragmentManager,
                                            @IdRes containerViewId: Int,
                                            fragment: Fragment,
                                            tag: String = "",
                                            @AnimatorRes enterAnimation: Int = 0,
                                            @AnimatorRes exitAnimation: Int = 0,
                                            @AnimatorRes popEnterAnimation: Int = 0,
                                            @AnimatorRes popExitAnimation: Int = 0,
                                            isAddFragment: Boolean = true,
                                            isAddToBackStack: Boolean = true,
                                            allowStateLoss: Boolean = false) {

    val fragmentTransaction = fragManager.beginTransaction()
            .setCustomAnimations(enterAnimation, exitAnimation, popEnterAnimation, popExitAnimation)

    if (isAddFragment) {
        fragmentTransaction.add(containerViewId, fragment, tag)
    } else {
        fragmentTransaction.replace(containerViewId, fragment, tag)
    }

    if (isAddToBackStack) {
        fragmentTransaction.addToBackStack(null)
    }

    if (allowStateLoss) {
        fragmentTransaction.commitAllowingStateLoss()
    } else {
        fragmentTransaction.commit()
    }
}
