package com.kotdroid.demo.sample.views.fragment.camerasample

import android.view.View
import android.widget.PopupMenu
import androidx.core.content.ContextCompat
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.viewmodels.BaseViewModel
import com.kotdroid.demo.sample.views.activity.CameraActivity
import com.kotdroid.demo.sample.views.activity.HomeActivity
import kotlinx.android.synthetic.main.fragment_video.*

class VideoFragment : BaseVideoFragment(), View.OnClickListener {

    override val layoutId: Int
        get() = R.layout.fragment_video

    override val isNavigationBarEnabled: Boolean
        get() = false

    override val viewModel: BaseViewModel?
        get() = null

    override fun setData() {

        // Set Click Listener
        ivRecordVideo.setOnClickListener(this)
        ivToggleCamera.setOnClickListener(this)
        ivVideoQuality.setOnClickListener(this)
        ivCameraIcon.setOnClickListener(this)
    }

    override fun observeData() {}

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.ivRecordVideo -> {
                if (isRecordingVideo) {
                    stopRecordingVideo()
                    ivRecordVideo.setImageDrawable(ContextCompat.getDrawable(activityContext,
                            R.drawable.ic_video_recording_stopped))
                } else {
                    startRecordingVideo()
                    ivRecordVideo.setImageDrawable(ContextCompat.getDrawable(activityContext,
                            R.drawable.ic_video_recording_ongoing))
                }
            }
            R.id.ivToggleCamera -> {
                toggleCamera()
            }
            R.id.ivVideoQuality -> {
                val popupMenu = PopupMenu(activityContext, view)
                popupMenu.inflate(R.menu.menu_video_quality)
                popupMenu.show()

                popupMenu.setOnMenuItemClickListener {
                    menuItemClick(it.itemId)
                    true
                }
            }
            R.id.ivCameraIcon -> {
                super.onPause()
                (activityContext as HomeActivity).openRequiredFragment(
                        HomeActivity.FRAGMENT_TYPE_CAMERA)
            }
        }
    }

    private fun menuItemClick(itemId: Int) {
        when (itemId) {
            R.id.menuLowQuality -> {
                audioBitRate = BaseVideoFragment.LOW_QUALITY_AUDIO_RATE
                videoBitRate = BaseVideoFragment.LOW_QUALITY_VIDEO_RATE
            }
            R.id.menuMediumQuality -> {
                audioBitRate = BaseVideoFragment.MEDIUM_QUALITY_AUDIO_RATE
                videoBitRate = BaseVideoFragment.MEDIUM_QUALITY_VIDEO_RATE
            }
            R.id.menuHighQuality -> {
                audioBitRate = BaseVideoFragment.HIGH_QUALITY_AUDIO_RATE
                videoBitRate = BaseVideoFragment.HIGH_QUALITY_VIDEO_RATE
            }
        }
    }

    override fun updateUi(time: String) {
        tvVideoRecordingTimer.text = time
        if (isRecordingVideo) {
            viewGroupVideo.visibility = View.GONE
        } else {
            viewGroupVideo.visibility = View.VISIBLE
        }
    }
}