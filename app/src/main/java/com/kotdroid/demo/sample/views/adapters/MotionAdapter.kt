package com.kotdroid.demo.sample.views.adapters

import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.views.activity.inflate

class MotionAdapter(val mFragment: Fragment) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MotionHolder(parent.inflate(layoutRes = R.layout.row_motion))
    }

    override fun getItemCount(): Int {
        return 20
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

    }

    internal class MotionHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}