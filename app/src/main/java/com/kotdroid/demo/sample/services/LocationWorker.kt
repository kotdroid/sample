package com.kotdroid.demo.sample.services

import android.annotation.SuppressLint
import android.util.Log
import android.widget.Toast
import androidx.work.Worker
import com.google.android.gms.location.LocationServices

class LocationWorker : Worker() {


    @SuppressLint("MissingPermission")
    override fun doWork(): Result {


        //Get location
        val mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(applicationContext)


        mFusedLocationProviderClient.lastLocation.addOnSuccessListener { it ->
            Log.e("WorkerSampleloc:", "lat : ${it?.latitude} lng :${it?.longitude}")
            Toast.makeText(applicationContext, "Worker- lat : ${it?.latitude} lng :${it?.longitude}", Toast.LENGTH_SHORT).show()
        }


        return Result.SUCCESS
    }
}