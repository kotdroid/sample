package com.kotdroid.demo.sample.views.activity

import android.content.Intent
import androidx.fragment.app.Fragment
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.views.fragment.camerasample.CameraFragment
import com.kotdroid.demo.sample.views.fragment.camerasample.HomeFragment
import com.kotdroid.demo.sample.views.fragment.camerasample.VideoFragment
import com.kotdroid.demo.sample.views.fragment.paypalsample.BrainTreeFragment

class HomeActivity : BaseAppCompactActivity() {

    companion object {

        const val FRAGMENT_TYPE_CAMERA = 1
        const val FRAGMENT_TYPE_VIDEO = 2
    }

    private val fragment = HomeFragment()

    override val layoutId: Int
        get() = R.layout.activity_home

    override val isMakeStatusBarTransparent: Boolean
        get() = false

    override fun init() {
        doFragmentTransaction(fragment = fragment, containerViewId = R.id.flContainerHome)
    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        if (requestCode == BrainTreeFragment.PAYPAL_REQUEST_CODE && null != data) {
//            fragment.onActivityResult(requestCode, resultCode, data)
//        } else  {
//            fragment.onActivityResult(requestCode, resultCode, data)
//        }
//    }

    fun openRequiredFragment(fragmentType: Int) {
        val fragment: Fragment = when (fragmentType) {
            CameraActivity.FRAGMENT_TYPE_CAMERA -> CameraFragment()
            CameraActivity.FRAGMENT_TYPE_VIDEO -> VideoFragment()
            else -> CameraFragment()
        }
        doFragmentTransaction(containerViewId = R.id.flContainerHome,
                isAddFragment = false,
                enterAnimation = R.animator.fade_in,
                popEnterAnimation = R.animator.fade_out,
                fragment = fragment)
    }
}