package com.kotdroid.demo.sample.views.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.views.activity.inflate
import kotlinx.android.synthetic.main.row_rv_text.view.*

/**
 * Created by SachinR on 7/5/2017.
 */
class CircularAdapter(private val context: Context, private val list: List<Int>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return Holder(parent.inflate(layoutRes = R.layout.row_rv_text))
    }

    @Suppress("NAME_SHADOWING")
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as Holder).bindHolder(position)
    }

    override fun getItemCount(): Int {
        return 6
    }

    private inner class Holder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                Toast.makeText(context, "Item no.${adapterPosition + 1} Clicked.", Toast.LENGTH_SHORT).show()
                //manager.smoothScrollToPosition(adapterPosition);
            }
        }

        @Suppress("NAME_SHADOWING")
        @SuppressLint("SetTextI18n")
        fun bindHolder(position: Int) {
            itemView.tvText.text = "Hello World " + (position + 1)
            itemView.ivImageView.setImageResource(list[position])
        }
    }
}
