package com.kotdroid.demo.sample.utils

import android.app.Application
import android.util.Log
import com.facebook.drawee.backends.pipeline.Fresco
import com.google.android.gms.tagmanager.ContainerHolder
import com.google.android.gms.tagmanager.TagManager
import com.kotdroid.demo.sample.R


class ApplicationGlobal : Application() {



    companion object {
        const val CONTAINER_ID = "GTM-NMS65WS"
        var time = ""
    }

    override fun onCreate() {
        super.onCreate()
        // Initialize fresco
        Fresco.initialize(this)


    }
}
