package com.kotdroid.demo.sample.views.fragment.circularmenusample

import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.models.pojos.Emoji
import com.kotdroid.demo.sample.viewmodels.BaseViewModel
import com.kotdroid.demo.sample.views.adapters.CircularEmojiAdapter
import com.kotdroid.demo.sample.views.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_sample_emoticons_copy.*
import kotlinx.android.synthetic.main.toolbar.*
import java.util.*

class SampleEmoticonFragment : BaseFragment() {

    companion object {
        private const val FIRST_EMOJI_ANGLE = 350f
        private const val LAST_EMOJI_ANGLE = 190f
        const val FIRST_ANGLE = 30f
        const val EMOJI_ANGLE_STEP = 40f

    }

    private val mEmojiAdapter by lazy {
        CircularEmojiAdapter(activityContext)
    }
    var list: ArrayList<Emoji>? = null
    override val layoutId: Int
        get() = R.layout.fragment_sample_emoticons_copy

    override val isNavigationBarEnabled: Boolean
        get() = false

    override val viewModel: BaseViewModel?
        get() = null

    override fun init() {

        // Set toolbar
        ivToolbarIcon.setImageResource(R.drawable.ic_logo_text)

        // Get list
        getList()

//        // Item TouchHelperCallback
//        val itemTouchHelperCallback = object : ItemTouchHelper.Callback() {
//            override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
//                return makeFlag(ItemTouchHelper.ACTION_STATE_DRAG,
//                        ItemTouchHelper.DOWN or
//                                ItemTouchHelper.UP or
//                                ItemTouchHelper.START or
//                                ItemTouchHelper.END)
//            }
//
//            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
//                Collections.swap(list, viewHolder.adapterPosition, target.adapterPosition)
//                mEmojiAdapter.notifyItemMoved(viewHolder.adapterPosition, target.adapterPosition)
//                return true
//            }
//
//            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
//
//            }
//
//        }
//
//        // Setting Circular RecyclerView
//        recyclerView.setCenterEdgeItems(true)
//        val manager = HalfCurveLayoutManager(activityContext, 5.0f)
//        recyclerView.layoutManager = manager
//        val helper = LinearSnapHelper()
//        mEmojiAdapter.updateList(list!!)
//        recyclerView.adapter = mEmojiAdapter
//        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView)
//        helper.attachToRecyclerView(recyclerView)




    }



    override fun observeData() {}

    private fun getList() {
        list = ArrayList<Emoji>().apply {
            add(Emoji(R.drawable.ic_happy, "Happy"))
            add(Emoji(R.drawable.ic_romentic, "Romantic"))
            add(Emoji(R.drawable.ic_neutral, "Neutral"))
            add(Emoji(R.drawable.ic_sad, "Sad"))
            add(Emoji(R.drawable.ic_calm, "Calm"))
            add(Emoji(R.drawable.ic_funny, "Funny"))
        }
    }

    private fun addEmoji(){
        for (i in 0..5) {

            // Get custom view
            val emojiView = LayoutInflater.from(activityContext).inflate(R.layout.row_emoji, null)
            emojiView.id = 100 + i

            // Set custom view
            val emojiViewPm = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT,
                    ConstraintLayout.LayoutParams.WRAP_CONTENT)
            emojiView.layoutParams = emojiViewPm
            rootLayout.addView(emojiView)
            emojiView.findViewById<ImageView>(R.id.ivEmojiIcon).setImageResource(list!![i].icon)
            emojiView.findViewById<TextView>(R.id.tvEmojiName).text = list!![i].name

            // Set Constraint and Clone
            val set = ConstraintSet()
            set.clone(rootLayout)
            when (i) {
                0 -> set.constrainCircle(emojiView.id, tvEmojiArea.id, resources.getDimension(R.dimen.emoji_path_radius).toInt(),
                        FIRST_EMOJI_ANGLE)
                5 -> set.constrainCircle(emojiView.id, tvEmojiArea.id, resources.getDimension(R.dimen.emoji_path_radius).toInt(), LAST_EMOJI_ANGLE)
                else -> set.constrainCircle(emojiView.id, tvEmojiArea.id, resources.getDimension(R.dimen.emoji_path_radius).toInt(), (FIRST_ANGLE + EMOJI_ANGLE_STEP * (i - 1)))
            }
            set.applyTo(rootLayout)
        }
    }
}