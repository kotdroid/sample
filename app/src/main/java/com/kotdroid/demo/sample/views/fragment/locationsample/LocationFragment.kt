package com.kotdroid.demo.sample.views.fragment.locationsample

import android.content.Intent
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.services.LocationUpdateService
import com.kotdroid.demo.sample.services.LocationWorker
import com.kotdroid.demo.sample.viewmodels.BaseLocationViewModel
import com.kotdroid.demo.sample.viewmodels.BaseViewModel
import com.kotdroid.demo.sample.viewmodels.HomeViewModel
import com.kotdroid.demo.sample.views.activity.BaseAppCompactActivity
import com.kotdroid.demo.sample.views.activity.doFragmentTransaction
import com.kotdroid.demo.sample.views.fragment.MotionFragment
import kotlinx.android.synthetic.main.fragment_location_home.*
import java.util.concurrent.TimeUnit

class LocationFragment : BaseLocationFragment(), View.OnClickListener {


    private lateinit var mHomeViewModel: HomeViewModel
    private lateinit var worker: PeriodicWorkRequest

    override val layoutId: Int
        get() = R.layout.fragment_location_home

    override val isNavigationBarEnabled: Boolean
        get() = false

    override val viewModel: BaseViewModel?
        get() = mHomeViewModel

    override fun setViewModel() {
        //Get ViewModel
        mHomeViewModel = ViewModelProviders.of(this)[HomeViewModel::class.java]
    }

    override fun setData() {

        worker = PeriodicWorkRequest.Builder(LocationWorker::class.java,
                5, TimeUnit.MINUTES).build()

        //trying to observe the work status but crashing here
        WorkManager.getInstance().getStatusById(worker.id).observeForever {
            if (it?.state?.isFinished!!) {
                Toast.makeText(activityContext, "Finished", Toast.LENGTH_SHORT).show()
            }
        }


        //Set OnClick Listener
        btnStartLocationUpdate.setOnClickListener(this)
        btnStopLocationUpdate.setOnClickListener(this)
        btnMotionLayout.setOnClickListener(this)

    }

    override fun observeData() {

        //observe user'sLocation
        mHomeViewModel.getUserLocation().observe(this, Observer {
            //do your current location related stuff here
            tvLocation.text = "Current Location Lat:${it?.latitude} Lng:${it?.longitude}"
        })
    }


    override fun getViewModel(): BaseLocationViewModel? {
        return mHomeViewModel
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnMotionLayout -> {
                (activityContext as BaseAppCompactActivity).doFragmentTransaction(
                        containerViewId = R.id.flContainerHome,
                        fragment = MotionFragment())
            }
            R.id.btnStartLocationUpdate -> {
                ContextCompat.startForegroundService(activityContext, Intent(activityContext, LocationUpdateService::class.java))
            }
            R.id.btnStopLocationUpdate -> {
                activityContext.stopService(Intent(activityContext, LocationUpdateService::class.java))


                /**
                 * If we are providing repeat interval less than 15 minutes that don't work,coz its
                 * minimum interval is 15 minutes and it will be executed once in 15 minutes .
                 * else at specified interval.
                 */
                //enqueuing location updates

                WorkManager.getInstance().enqueueUniquePeriodicWork("LocationUpdate",
                        ExistingPeriodicWorkPolicy.REPLACE, worker)

//            WorkManager.getInstance().enqueue(PeriodicWorkRequest.Builder(
//                    LocationWorker::class.java,
//                    5,
//                    TimeUnit.MINUTES
//            ).build())
            }

        }
    }
}