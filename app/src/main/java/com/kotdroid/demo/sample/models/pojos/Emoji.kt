package com.kotdroid.demo.sample.models.pojos

data class Emoji(var icon: Int, var name: String)