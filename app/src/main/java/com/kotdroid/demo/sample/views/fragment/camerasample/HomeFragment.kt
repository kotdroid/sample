package com.kotdroid.demo.sample.views.fragment.camerasample

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.provider.MediaStore
import android.view.View
import android.view.ViewGroup
import com.github.tcking.giraffecompressor.GiraffeCompressor
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.utils.Constants
import com.kotdroid.demo.sample.utils.GetSampledVideo
import com.kotdroid.demo.sample.utils.MarshMallowPermissions
import com.kotdroid.demo.sample.viewmodels.BaseViewModel
import com.kotdroid.demo.sample.views.activity.BaseAppCompactActivity
import com.kotdroid.demo.sample.views.activity.doFragmentTransaction
import com.kotdroid.demo.sample.views.activity.inflate
import com.kotdroid.demo.sample.views.fragment.videotrimsample.GetTranscodedVideo
import com.kotdroid.demo.sample.views.fragment.videotrimsample.Transcoder.TranscodingClass
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.show_picture_options_bottom_sheet.view.*

class HomeFragment : BaseCameraFragment(), View.OnClickListener, GetSampledVideo.SampledVideoAsyncResp, GetTranscodedVideo {

    companion object {
        private const val GALLERY_REQUEST = 234
        private const val CAMERA_REQUEST = 23
    }

    private var picturePath: String? = null
    private var imagesDirectory: String? = null
    private var isCameraOptionSelected: Boolean = false
    private val marshMallowPermissions by lazy { MarshMallowPermissions(this) }

    override val layoutId: Int
        get() = R.layout.fragment_home

    override val isNavigationBarEnabled: Boolean
        get() = false

    override val viewModel: BaseViewModel?
        get() = null

    override fun setData() {

        btnUploadFile.setOnClickListener(this)

        GiraffeCompressor.init(context);

    }


    override fun observeData() {}


    @Suppress("NAME_SHADOWING")
    override fun onClick(view: View?) {
        val imagesDirectory = Constants.LOCAL_STORAGE_BASE_PATH_FOR_USER_VIDEOS
        when (view?.id) {
            R.id.btnUploadFile -> {

                val bottomSheetDialog = BottomSheetDialog(activityContext)
                val view = (getView() as ViewGroup).inflate(layoutRes = R.layout.show_picture_options_bottom_sheet)

                view.tvCamera.setOnClickListener {
                    checkForPermissions(true, imagesDirectory)
                    bottomSheetDialog.dismiss()
                }
                view.tvGallery.setOnClickListener {
                    checkForPermissions(false, imagesDirectory)
                    bottomSheetDialog.dismiss()
                }
                view.tvCancel.setOnClickListener { bottomSheetDialog.dismiss() }

                bottomSheetDialog.setContentView(view)
                bottomSheetDialog.show()
            }
        }
    }

    private fun checkForPermissions(isCameraOptionSelected: Boolean, imagesDirectory: String) {
        this.isCameraOptionSelected = isCameraOptionSelected
        this.imagesDirectory = imagesDirectory

        if (marshMallowPermissions.isPermissionGrantedForWriteExtStorage) {
            if (isCameraOptionSelected) {
                (activityContext as BaseAppCompactActivity).doFragmentTransaction(containerViewId = R.id.flContainerHome,
                        enterAnimation = R.animator.fade_in,
                        popEnterAnimation = R.animator.fade_out,
                        fragment = VideoFragment())
            } else {
                openGallery()
            }
        } else {
            marshMallowPermissions.requestPermisssionForWriteStorage()
        }
    }

    private fun openGallery() {
        val intent = Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "*/*"
        startActivityForResult(intent, GALLERY_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && (requestCode == GALLERY_REQUEST)) {
            var isGalleryImage = false
            if (requestCode == GALLERY_REQUEST) {
                val selectedImage = data!!.data
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                val cursor = activity!!.contentResolver.query(selectedImage,
                        filePathColumn, null, null, null)
                cursor!!.moveToFirst()
                val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                picturePath = cursor.getString(columnIndex)
                cursor.close()
                isGalleryImage = true
                try {
                    if (picturePath != null) {

                        val mp = MediaPlayer.create(activityContext, Uri.parse(picturePath))
                        val duration = mp.duration
                        mp.release()

                        if (duration / 1000 > 60) {
                            // Show Your Messages
                            showMessage(message = "Video length is more than 1 minute.",
                                    isShowSnackbarMessage = true)
                        } else {
//                            GiraffeCompressor.create()
//                                    .input(File(picturePath))
//                                    .output(GeneralFunctions.setUpVideoFile(Constants.LOCAL_STORAGE_BASE_PATH_FOR_USER_VIDEOS))
//                                    .bitRate(12800)
//                                    .resizeFactor(1f)
//                                    .ready()
//                                    .observeOn(AndroidSchedulers.mainThread())
//                                    .subscribe(object : Subscriber<GiraffeCompressor.Result>() {
//                                        override fun onNext(t: GiraffeCompressor.Result?) {
//                                            showMessage(message = "Timing ${t?.costTime} Output File ${t?.output}")
//                                        }
//
//                                        override fun onCompleted() {
//                                        }
//
//                                        override fun onError(e: Throwable?) {
//                                        }
//                                    })


//                            GetSampledVideo(this).execute(picturePath,
//                                    GeneralFunctions.createVideoDirectory(Constants.LOCAL_STORAGE_BASE_PATH_FOR_USER_VIDEOS))

//                            MediaController.getInstance().convertVideo(picturePath,
//                                    Constants.LOCAL_STORAGE_BASE_PATH_FOR_USER_VIDEOS)

//                            SiliCompressor.with(activityContext).compressVideo(picturePath, imagesDirectory)

                            val transcoding = TranscodingClass(this)
                            transcoding.setFile(picturePath)
                            transcoding.startTranscode(ProgressDialog.show(activityContext, "Video Compression", "Compressing"))

                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

//            GetSampledImage(this).execute(picturePath, imagesDirectory,
//                    isGalleryImage.toString(),
//                    resources.getDimension(R.dimen.user_image_downsample_size).toInt().toString())
        }
    }

    override fun onSampledVideoAsyncPostExecute(path: String) {
        showMessage(message = "Video Converted : $path")
    }

    override fun getTranscoded(fileVideo: String?, isPass: Boolean?) {
        showMessage(message = "Saved at :$fileVideo")
    }

}