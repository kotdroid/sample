package com.kotdroid.demo.sample.utils

import android.os.AsyncTask
import androidx.fragment.app.Fragment

class AsyncVideoSampling(private val mFragment: Fragment) : AsyncTask<String, Int, String>() {

    private var mSampledVideoAsyncResp: SampledVideoAsyncResp? = null

    //private val mMyCustomLoader: MyCustomLoader by lazy { MyCustomLoader(mFragment.context) }

    init {
        mSampledVideoAsyncResp = mFragment as SampledVideoAsyncResp
    }

    override fun onPreExecute() {
        super.onPreExecute()
        // mMyCustomLoader.showProgressDialog("Sampling")
    }

    override fun doInBackground(vararg params: String): String {

//        val inputFile = params[0]
//        val outputFile = params[1]
//        val vi = VideoDownSampling()
//        try {
//            vi.changeResolution(File(inputFile), outputFile)
//            onProgressUpdate(vi.percentage)
//        } catch (e: Exception) {
//
//        }

        return ""
    }

    override fun onProgressUpdate(vararg values: Int?) {
        super.onProgressUpdate(*values)

    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
        // mMyCustomLoader.dismissProgressDialog()
        mSampledVideoAsyncResp?.onSampledVideoAsyncPostExecute(result)

    }

    interface SampledVideoAsyncResp {
        fun onSampledVideoAsyncPostExecute(outputFilePath: String?)
    }
}