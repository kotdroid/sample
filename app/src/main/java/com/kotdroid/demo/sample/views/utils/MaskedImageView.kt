package com.kotdroid.demo.sample.views.utils

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import android.graphics.drawable.NinePatchDrawable
import android.util.AttributeSet
import android.widget.ImageView


class MaskedImageView : ImageView {
    private var srcBitmap: Bitmap? = null
    private var foregroundDrawable: Drawable? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    @SuppressLint("DrawAllocation")
    override fun onDraw(onDrawCanvas: Canvas) {
        val mutableBitmap = Bitmap.createBitmap(measuredWidth, measuredHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(mutableBitmap)

        val paint = Paint()
        paint.isFilterBitmap = false

        if (srcBitmap != null) {
            canvas.drawBitmap(srcBitmap!!, 0f, 0f, paint)
        }

        if (background != null) {
            val background = background as NinePatchDrawable
            background.paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.DST_IN)
            background.draw(canvas)
        }

        val foreground = foregroundDrawable
        if (foreground != null) {
            foreground.setBounds(0, 0, getRight() - getLeft(), getBottom() - getTop())

            val scrollX = scrollX
            val scrollY = scrollY

            if (scrollX or scrollY == 0) {
                foreground.draw(canvas)
            } else {
                canvas.translate(scrollX.toFloat(), scrollY.toFloat())
                foreground.draw(canvas)
                canvas.translate(-scrollX.toFloat(), -scrollY.toFloat())
            }
        }

        onDrawCanvas.drawBitmap(mutableBitmap, 0f, 0f, paint)
    }

    override fun setImageBitmap(bitmap: Bitmap) {
        super.setImageBitmap(bitmap)
        srcBitmap = bitmap

        invalidate()
    }

    fun setForegroundResource(resId: Int) {
        setForegroundDrawable(context.resources.getDrawable(resId))
    }

    fun setForegroundDrawable(d: Drawable) {
        d.callback = this
        d.setVisible(getVisibility() === VISIBLE, false)

        foregroundDrawable = d

        requestLayout()
        invalidate()
    }
}