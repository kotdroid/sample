package com.kotdroid.demo.sample.views.activity

import androidx.fragment.app.Fragment
import com.kotdroid.demo.sample.R
import com.kotdroid.demo.sample.views.fragment.camerasample.CameraFragment
import com.kotdroid.demo.sample.views.fragment.camerasample.VideoFragment

class CameraActivity : BaseAppCompactActivity() {

    companion object {

        const val FRAGMENT_TYPE_CAMERA = 1
        const val FRAGMENT_TYPE_VIDEO = 2
    }

    var fragment: Fragment = CameraFragment()
    override val layoutId: Int
        get() = R.layout.activity_home

    override val isMakeStatusBarTransparent: Boolean
        get() = true

    override fun init() {

        // Open CameraFragment by default
        openRequiredFragment(FRAGMENT_TYPE_CAMERA)
    }

    fun openRequiredFragment(fragmentType: Int) {
        fragment = when (fragmentType) {
            FRAGMENT_TYPE_CAMERA -> CameraFragment()
            FRAGMENT_TYPE_VIDEO -> VideoFragment()
            else -> CameraFragment()
        }
        doFragmentTransaction(containerViewId = R.id.flContainerHome,
                isAddFragment = false,
                enterAnimation = R.animator.fade_in,
                popEnterAnimation = R.animator.fade_out,
                fragment = fragment)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.fragments.size < 1) {
            finish()
        }
    }


}